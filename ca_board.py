from ca_cell import *
import numpy as np
import copy
from ca_colors import Colors


class Board:
    def __init__(self, width=500, hight=500):
        self.board_width = width
        self.board_hight = hight
        self.board = []
        self.init_board()  # board of cells

    def init_board(self):
        temp = []
        for x_pos in range(0, self.board_width):
            column = []
            for y_pos in range(0, self.board_hight):
                column.append(Cell())
            temp.append(column)
        self.board = temp

    def print_board_grain_id(self):
        matrix = np.zeros((self.board_hight, self.board_width), dtype=int)  # np.zeros takes: (hight, width)
        for x_pos, col in enumerate(self.board):
            for y_pos, cell in enumerate(col):
                matrix[y_pos][x_pos] = cell.get_grain_id()
        print(matrix)

    def print_board_incl_id(self):
        matrix = np.zeros((self.board_hight, self.board_width), dtype=int)  # np.zeros takes: (hight, width)
        for x_pos, col in enumerate(self.board):
            for y_pos, cell in enumerate(col):
                matrix[y_pos][x_pos] = cell.get_inclusion_id()
        print(matrix)

    def print_board_phase_id(self):
        matrix = np.zeros((self.board_hight, self.board_width), dtype=int)  # np.zeros takes: (hight, width)
        for x_pos, col in enumerate(self.board):
            for y_pos, cell in enumerate(col):
                matrix[y_pos][x_pos] = cell.get_phase_id()
        print(matrix)

    def set_cell_grain_id(self, x_pos, y_pos, id_value):
        self.board[x_pos][y_pos].set_grain_id(id_value)

    def get_cell_grain_id(self, x_pos, y_pos):
        try:
            return self.board[x_pos][y_pos].get_grain_id()
        except:
            print("x_pos =", x_pos, ", y_pos =", y_pos)

    def set_cell_phase_id(self, x_pos, y_pos, id_value):
        self.board[x_pos][y_pos].set_phase_id(id_value)

    def get_cell_phase_id(self, x_pos, y_pos):
        return self.board[x_pos][y_pos].get_phase_id()

    def set_cell_inclusion_id(self, x_pos, y_pos, id_value):
        self.board[x_pos][y_pos].set_inclusion_id(id_value)

    def get_cell_inclusion_id(self, x_pos, y_pos):
        return self.board[x_pos][y_pos].get_inclusion_id()

    # TODO
    def clear_board(self):
        pass

    # TODO
    def set_board(self, board):
        self.board = board

    def get_copy(self):
        return copy.deepcopy(self.board)

    def get_width(self):
        return self.board_width

    def get_hight(self):
        return self.board_hight

    def set_width(self, board_width):
        self.board_width = board_width
        self.init_board()

    def set_hight(self, board_hight):
        self.board_hight = board_hight
        self.init_board()

    def get_cell_color(self, x_pos, y_pos, color: Colors):
        return color.get_color_by_cell(self.board[x_pos][y_pos])

# my_board = Board(20,10)
# my_board.set_cell_grain_id(0,1,22)
# my_board.set_cell_grain_id(4,7,21)
#
# my_board.print_board()

# print(my_board.get_cell_grain_id(0,1))
# print(my_board.get_cell_grain_id(1,1))
