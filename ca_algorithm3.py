import random
from typing import List, Tuple, Dict
import copy
import numpy as np

# MOORE_NEIGHBOURHOOD = ((-1,-1), (0,-1),  (+1,-1),
#                        (-1,0),           (+1,0),
#                        (-1,+1), (0,+1),  (+1,+1))
#
# VONNEUMANN_NEIGHBOURHOOD = (         (0,-1),
#                             (-1,0),           (+1,0),
#                                      (0,+1)         )

#
# #The function rands "incl_cnt" of inclusions, and adds them with specified radius
# def matrix_incl_rand(my_matrix: List[List], incl_cnt: int, radius_min: float, radius_max: float) -> List[Tuple]:
#     #print("Enetered: matrix_random_init")
#     incl_id = 1
#     initiated_pos = []
#     fin_initiated_pos = []
#     initiated_radius = []
#     len_width = len(my_matrix)
#     len_hight = len(my_matrix[0])
#     i=0
#     while(i<incl_cnt):
#         pos_width = random.randrange(0,len_width)
#         pos_hight = random.randrange(0,len_hight)
#         # print(f"x_pos={pos_width}, y_pos={pos_hight}")
#         if [pos_width,pos_hight] not in initiated_pos:
#             i+=1
#             initiated_pos.append((pos_width,pos_hight))
#             initiated_radius.append(random.uniform(radius_min,radius_max))
#             my_matrix[pos_width][pos_hight] = incl_id
#
#     for y_pos in range(len_hight):
#         for x_pos in range(len_width):
#             if my_matrix[x_pos][y_pos] == 0:
#                 for init_pos, init_rad in zip(initiated_pos, initiated_radius):
#                     init_width, init_hight = init_pos
#                     if pow((x_pos-init_width),2) + pow((y_pos-init_hight),2) <= pow(init_rad,2):
#                         fin_initiated_pos.append((x_pos,y_pos))
#                         my_matrix[x_pos][y_pos] = incl_id
#     return fin_initiated_pos + initiated_pos

# #rands colors and returns a
# def color_dict_rand(colors_cnt: int) -> Dict:
#     colors = dict()
#     added_colors = 2
#     colors.update({0: (255, 255, 255)})                         #default color has to be always white
#     colors.update({1: (0, 0, 0)})                               #phase color is black
#     for x in range(colors_cnt):
#         colors.update({(x+added_colors): (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))})
#     return colors
#
# def color_distrib(value_of_cell, colours: Dict) -> Tuple:
#     #print("Entered: color_distrib")
#     return colours.get(value_of_cell, None)

# #new function random init to change with the proper one in ca_algorithm
# def matrix_random_init(my_matrix: List[List], color_cnt: int, initiated_incl: List[Tuple]) -> List[Tuple]:
#     #print("Enetered: matrix_random_init")
#     print(initiated_incl)
#     initiated_pos = []
#     # columns = len(my_matrix)
#     len_width = len(my_matrix)
#     # rows = len(my_matrix[0])
#     len_hight = len(my_matrix[0])
#     inc_cnt = 1
#     id_number=1
#     while(id_number<color_cnt+inc_cnt):
#         temp_width = random.randrange(0,len_width)
#         temp_hight = random.randrange(0,len_hight)
#         if (temp_width,temp_hight) not in initiated_pos and (temp_width,temp_hight) not in initiated_incl:
#             id_number += 1
#             initiated_pos.append((temp_width,temp_hight))
#             my_matrix[temp_width][temp_hight] = id_number
#     return initiated_pos

#This function is to check which id is the most frequent and return it
def grain_ID_distrib(id_list: List) -> int:
    NO_CELL = 0
    set_id = 0
    most_frequent = 0
    unique_vals = set(id_list)
    if len(id_list) > 0:
        for val in unique_vals:
            if val != NO_CELL:
                frequence = id_list.count(val)
                #print(frequence)
                if frequence == most_frequent:
                    set_id = random.choice([set_id, val])         #if there is the same frequence of 2 IDs choice one
                elif frequence > most_frequent:
                    most_frequent = frequence
                    set_id = val                                #set the most frequent ID
    return set_id


def grain_next_gen(my_matrix: List[List], nei_method: Tuple[Tuple], sharp_border: bool) -> int:
    NO_CELL_ID = 0
    INCL_ID = 1
    old_matrix = copy.deepcopy(my_matrix)
    len_width = len(my_matrix)
    len_hight = len(my_matrix[0])

    empty_cnt = 0
    #print("sharp_border is: ",sharp_border)
    ## Algorytm next_gen
    for grain_pos_hight in range(len_hight):
        for grain_pos_width in range(len_width):
            #print(f"PRAWILNE COORDY {grain_pos_width}, {grain_pos_hight}  ")
            analyzed_nei = list()
            actual_id = old_matrix[grain_pos_width, grain_pos_hight]
            if NO_CELL_ID == actual_id:
                empty_cnt += 1
                for nei_cell in nei_method:                                                                             #for every single neibourhood cell
                    nei_width = (grain_pos_width + nei_cell[0])
                    nei_hight = (grain_pos_hight + nei_cell[1])
                    flag_sb_in_range = 1
                    if sharp_border is True:
                        if nei_width < 0 or nei_width >= len_width or nei_hight < 0 or nei_hight >= len_hight:
                            flag_sb_in_range = 0
                    else:                                                                                               #acts like torus when analyzed cell is out of the matrix
                        nei_width = nei_width % len_width
                        nei_hight = nei_hight % len_hight

                    if flag_sb_in_range == 0:
                        analyzed_nei.append(0)
                    else:
                        nei_cell_val = old_matrix[nei_width, nei_hight]
                        if nei_cell_val == INCL_ID:
                            continue
                        else:
                            analyzed_nei.append(nei_cell_val)
                my_matrix[grain_pos_width, grain_pos_hight] = grain_ID_distrib(analyzed_nei)                            #set most frequent ID
    return empty_cnt

def matrix_init(width: int, hight: int):
    return np.zeros((width, hight), dtype=int)


# def matrix_phase_rand(my_matrix: List[List], phase_cnt: int, id_number: int) -> List[List]:
#     #print("Enetered: matrix_random_init")
#     initiated_pos = []
#     columns = len(my_matrix)
#     rows = len(my_matrix[0])
#     i = 0
#     while(i<phase_cnt):
#         temp_col = random.randrange(0,columns)
#         temp_row = random.randrange(0,rows)
#         if [temp_col,temp_row] not in initiated_pos:
#             initiated_pos.append([temp_col,temp_row])
#             my_matrix[temp_col][temp_row] = id_number
#             i+=1
#     #print("--->matrix_random_init: initiation done")
#     return initiated_pos

# rands x_pos and y_pos in range of x_pos_max and y_pos_max
def rand_position(x_pos_max: int, y_pos_max: int):
    return random.randrange(0, x_pos_max), random.randrange(0, y_pos_max)


# def matrix_combine(matrix_seeds: List[List], matrix_phases: List[List]) -> List[List[Tuple]]:
#     temp_width = len(matrix_seeds)
#     temp_hight = len(matrix_seeds[0])
#     combined_matrix = []
#     if temp_width is len(matrix_phases) and temp_hight is len(matrix_phases[0]):
#         for y_pos in range(temp_hight):
#             new_row = []
#             for x_pos in range(temp_width):
#                 new_row.append( (matrix_seeds[x_pos][y_pos], matrix_phases[x_pos][y_pos]) )
#             combined_matrix.append(new_row)
#         return combined_matrix
#     else:
#         return []

def matrix_combine(matrix_seeds: List[List], matrix_phases: List[List]) -> tuple:
    return (matrix_seeds, matrix_phases)



# my_matrix = matrix_init(500,500)
# colors = color_dict_rand(30)
# inclusions = matrix_incl_rand(my_matrix,30,40)
# matrix_random_init(my_matrix,500,inclusions)
#
# prev = grain_next_gen(my_matrix, VONNEUMANN_NEIGHBOURHOOD, sharp_border=True)
# import ca_io_operations as caio
# caio.show_PNG_map(my_matrix, colors)
#
#
# #
# print("dupa")
# my_matrix_phase = matrix_init(10,10)
# my_matrix_seeds = matrix_init(10,10)
# print("dupa")
# init_pos_inclusions =  matrix_phase_rand(my_matrix_phase, 90, 3)
# print("dupa")
# print(my_matrix_phase)
# print("---------------->", init_pos_inclusions)
#
# init_pos_seed = matrix_random_init(my_matrix_seeds, 5, init_pos_inclusions)
#
# print(my_matrix_seeds)
#
# combined_matrix = matrix_combine(my_matrix_seeds, my_matrix_phase)
# #print(combined_matrix)








