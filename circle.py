from typing import List, Tuple
import random
import ca_algorithm as ca
import ca_io_operations as caio
import numpy as np
import copy



#The function rands "incl_cnt" of inclusions, and adds them with specified radius
def matrix_incl_rand(my_matrix: List[List], incl_cnt: int, radius: float) -> List[Tuple]:
    #print("Enetered: matrix_random_init")
    incl_id = 1
    MIN_RADIUS = 0.5
    initiated_pos = []
    fin_initiated_pos = []
    initiated_radius = []
    len_width = len(my_matrix)
    len_hight = len(my_matrix[0])
    i=0
    while(i<incl_cnt):
        pos_width = random.randrange(0,len_width)
        pos_hight = random.randrange(0,len_hight)
        # print(f"x_pos={pos_width}, y_pos={pos_hight}")
        if [pos_width,pos_hight] not in initiated_pos:
            i+=1
            initiated_pos.append((pos_width,pos_hight))
            initiated_radius.append(random.uniform(MIN_RADIUS,radius))
            my_matrix[pos_width][pos_hight] = incl_id

    for y_pos in range(len_hight):
        for x_pos in range(len_width):
            if my_matrix[x_pos][y_pos] == 0:
                for init_pos, init_rad in zip(initiated_pos, initiated_radius):
                    init_width, init_hight = init_pos
                    if pow((x_pos-init_width),2) + pow((y_pos-init_hight),2) <= pow(init_rad,2):
                        fin_initiated_pos.append((x_pos,y_pos))
                        my_matrix[x_pos][y_pos] = incl_id
    return fin_initiated_pos + initiated_pos

#new function random init to change with the proper one in ca_algorithm
def matrix_random_init(my_matrix: List[List], color_cnt: int, initiated_incl: List[Tuple]) -> List[Tuple]:
    #print("Enetered: matrix_random_init")
    print(initiated_incl)
    initiated_pos = []
    # columns = len(my_matrix)
    len_width = len(my_matrix)
    # rows = len(my_matrix[0])
    len_hight = len(my_matrix[0])
    inc_cnt = 1
    id_number=1
    while(id_number<color_cnt+inc_cnt):
        temp_width = random.randrange(0,len_width)
        temp_hight = random.randrange(0,len_hight)
        if (temp_width,temp_hight) not in initiated_pos and (temp_width,temp_hight) not in initiated_incl:
            id_number += 1
            initiated_pos.append((temp_width,temp_hight))
            my_matrix[temp_width][temp_hight] = id_number
    return initiated_pos

def grain_next_gen(my_matrix: List[List], nei_method: Tuple[Tuple], sharp_border: bool) -> int:
    NO_CELL_ID = 0
    INCL_ID = 1
    old_matrix = copy.deepcopy(my_matrix)
    len_width = len(my_matrix)
    len_hight = len(my_matrix[0])

    empty_cnt = 0
    #print("sharp_border is: ",sharp_border)
    ## Algorytm next_gen
    for grain_pos_hight in range(len_hight):
        for grain_pos_width in range(len_width):
            #print(f"PRAWILNE COORDY {grain_pos_width}, {grain_pos_hight}  ")
            analyzed_nei = list()
            actual_id = old_matrix[grain_pos_width, grain_pos_hight]
            if NO_CELL_ID == actual_id:
                empty_cnt += 1
                for nei_cell in nei_method:                                                                             #for every single neibourhood cell
                    nei_width = (grain_pos_width + nei_cell[0])
                    nei_hight = (grain_pos_hight + nei_cell[1])
                    flag_sb_in_range = 1
                    if sharp_border is True:
                        if nei_width < 0 or nei_width >= len_width or nei_hight < 0 or nei_hight >= len_hight:
                            flag_sb_in_range = 0
                    else:                                                                                               #acts like torus when analyzed cell is out of the matrix
                        nei_width = nei_width % len_width
                        nei_hight = nei_hight % len_hight

                    if flag_sb_in_range == 0:
                        analyzed_nei.append(0)
                    else:
                        nei_cell_val = old_matrix[nei_width, nei_hight]
                        if nei_cell_val == INCL_ID:
                            continue
                        else:
                            analyzed_nei.append(nei_cell_val)
                my_matrix[grain_pos_width, grain_pos_hight] = ca.grain_ID_distrib(analyzed_nei)                            #set most frequent ID
    return empty_cnt

seed_cnt = 2000
color_dict = ca.color_dict_incl_rand(seed_cnt)

# print(color_dict)
# new_matrix = np.zeros((10,10),int)
# print(new_matrix)
#  my_matrix = [[1,1,1,1,1,1,1,1,1,1,1],
# [1,1,1,1,1,1,1,1,1,1,1],
# [1,1,1,1,1,1,1,1,1,1,1],
# [1,1,1,1,1,0,0,1,1,1,1],
# [1,1,1,1,0,0,0,1,1,1,1],
# [2,2,2,0,0,0,2,2,2,2,2],
# [2,2,2,0,0,2,2,2,2,2,2],
# [2,2,2,2,2,2,2,2,2,2,2],
# [2,2,2,2,2,2,2,2,2,2,2],
# [2,2,2,2,2,2,2,2,2,2,2],
# [2,2,2,2,2,2,2,2,2,2,2],
# [2,2,2,2,2,2,2,2,2,2,2]
# ]

my_matrix = np.zeros((500,500), dtype=int)
print(my_matrix)
#
# my_matrix = [[0,0,0,0,0,0,0,0,0,0,0],
# [0,0,0,0,0,0,0,0,0,0,0],
# [0,0,0,0,0,0,0,0,0,0,0],
# [0,0,0,0,0,0,0,0,0,0,0],
# [0,0,0,0,0,0,0,0,0,0,0],
# [0,0,0,0,0,0,0,0,0,0,0],
# [0,0,0,0,0,0,0,0,0,0,0],
# [0,0,0,0,0,0,0,0,0,0,0],
# [0,0,0,0,0,0,0,0,0,0,0],
# [0,0,0,0,0,0,0,0,0,0,0],
# [0,0,0,0,0,0,0,0,0,0,0],
# [0,0,0,0,0,0,0,0,0,0,0]
# ]

inclusions =  matrix_incl_rand(my_matrix, incl_cnt=50, radius=6.2)
#matrix_random_init(my_matrix,6)
print(my_matrix)

matrix_random_init(my_matrix,seed_cnt,inclusions)


# matrix_incl_rand(my_matrix,incl_cnt=3)
#caio.show_PNG_map(my_matrix,color_dict)
print(my_matrix)

while 1:
    prev = grain_next_gen(my_matrix, ca.VONNEUMANN_NEIGHBOURHOOD, sharp_border=True)
    caio.show_PNG_map(my_matrix, color_dict)
    now = grain_next_gen(my_matrix, ca.VONNEUMANN_NEIGHBOURHOOD, sharp_border=True)
    caio.show_PNG_map(my_matrix,color_dict)
    if prev == now:
        break


# print(my_matrix_got)
# print(cnt)