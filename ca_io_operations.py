from typing import Dict, Tuple, List
import random
from PIL import Image
import os
import ca_algorithm as ca
import csv
from tkinter import Tk
from tkinter.filedialog import askopenfilename
import numpy as np
from ca_sim import CASim


def export_sim_to_csv(ca_sim: CASim):
    path_to_file = os.getcwd() + r"\csv%s.txt"
    path_to_file = get_path_to_save_file(path_to_file)
    with open(path_to_file, mode='w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

        width = ca_sim.get_painting_width()
        hight = ca_sim.get_painting_hight()
        csv_writer.writerow(["width", "hight", "seed_cnt", "incl_cnt"])
        csv_writer.writerow([width, hight, ca_sim.get_seed_cnt(), ca_sim.get_inclusion_cnt()])
        csv_writer.writerow(["x_pos", "y_pos", "grain_id", "incl_id", "phase_id"])

        for j in range(0, hight):
            for i in range(0, width):
                csv_writer.writerow(
                    [i, j, ca_sim.board.get_cell_grain_id(i, j), ca_sim.board.get_cell_inclusion_id(i, j),
                     ca_sim.board.get_cell_phase_id(i, j)])


def import_from_csv():
    ca_sim = None
    Tk().withdraw()                 # we don't want a full GUI, so keep the root window from appearing
    filename = askopenfilename()    # show an "Open" dialog box and return the path to the selected file
    # print("filename:", filename)
    if str(filename).strip(" ") is not "":
        # print("weszłem")
        with open(filename, mode='r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if len(row) > 1:
                    if line_count == 0:
                        pass
                        # print(f'Column names are {", ".join(row)}')
                    elif line_count == 1:
                        # print(f'Values are: {", ".join(row)}')
                        ca_sim = CASim(width=int(row[0]), hight=int(row[1]), seed_cnt=int(row[2]), incl_cnt=int(row[3]))
                        # print(f'x_pos_max={row[0]}, y_pos_max={row[1]}, grain_id_cnt={row[2]}.')
                    elif line_count == 2:
                        pass
                        # print(f'Column names are {", ".join(row)}')
                    else:
                        # my_matrix[int(row[0]),int(row[1])] = int(row[2])
                        x_pos = int(row[0])
                        y_pos = int(row[1])
                        ca_sim.board.set_cell_grain_id(x_pos, y_pos, int(row[2]))
                        ca_sim.board.set_cell_inclusion_id(x_pos, y_pos, int(row[3]))
                        ca_sim.board.set_cell_phase_id(x_pos, y_pos, int(row[4]))
                        # print(f'x_pos={row[0]}, y_pos={row[1]}, grain_id={row[2]}.')
                    line_count += 1
            print(f'Processed {line_count} lines.')
    return ca_sim

class CASimIO:
    def __init__(self):
        self.ca_sim = CASim()


def get_path_to_save_file(path: str):
    path_to_save = path
    i = 1
    while os.path.exists(path_to_save % i):
        i += 1
    path_to_save = path_to_save % i
    # print(path_to_save)
    return path_to_save


def create_PNG_map(my_matrix: List[List], color_dict: Dict):
    path_to_save = os.getcwd() + r"\snapshots\snapshot%s.png"
    path_to_save = get_path_to_save_file(path_to_save)

    temp_width = len(my_matrix)
    temp_hight = len(my_matrix[0])
    img = Image.new('RGB', (temp_width, temp_hight), "black")
    pixels = img.load()
    for j in range(0,temp_hight):
        for i in range(0,temp_width):
            new_rgb = ca.color_distrib(my_matrix[i][j], color_dict)
            if new_rgb == None:
                new_rgb = (255,255,255)
            pixels[i,j] = new_rgb
            #print(new_RGB)
    img.show()
    img.save(path_to_save)
    #print(color_dict)

def show_PNG_map(my_matrix: List[List], color_dict: Dict):
    path_to_save = os.getcwd() + r"\snapshots\snapshot%s.png"
    path_to_save = get_path_to_save_file(path_to_save)

    temp_width = len(my_matrix)
    temp_hight = len(my_matrix[0])
    img = Image.new('RGB', (temp_width, temp_hight), "black")
    pixels = img.load()
    for j in range(0,temp_hight):
        for i in range(0,temp_width):
            new_rgb = ca.color_distrib(my_matrix[i][j], color_dict)
            if new_rgb == None:
                new_rgb = (255,255,255)
            pixels[i,j] = new_rgb
            #print(new_RGB)
    img.show()

# def export_to_csv(my_matrix: List[List], grain_id_cnt: int) :
#     path_to_file = os.getcwd() + r"\csv%s.txt"
#     path_to_file = get_path_to_save_file(path_to_file)
#
#     with open(path_to_file, mode='w') as csv_file:
#         csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE)
#
#         csv_writer.writerow(["x_pos_max", "y_pos_max", "grain_id_count"])
#         csv_writer.writerow([len(my_matrix), len(my_matrix[0]), grain_id_cnt])
#         csv_writer.writerow(["x_pos", "y_pos", "grain_id"])
#
#         temp_width = len(my_matrix)
#         temp_hight = len(my_matrix[0])
#         img = Image.new('RGB', (temp_width, temp_hight), "black")
#         pixels = img.load()
#         for j in range(0,temp_hight):
#             for i in range(0,temp_width):
#                 csv_writer.writerow([i, j, my_matrix[i][j]])


# def import_from_csv() -> (List[List], int):
#     Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing
#     filename = askopenfilename()  # show an "Open" dialog box and return the path to the selected file
#     print("filename:",filename)
#     if str(filename).strip(" ") is not "":
#         print("weszłem")
#         with open(filename, mode='r') as csv_file:
#             csv_reader = csv.reader(csv_file,delimiter=',')
#             line_count = 0
#             for row in csv_reader:
#                 if len(row)>1:
#                     if line_count == 0:
#                         pass
#                         #print(f'Column names are {", ".join(row)}')
#                     elif line_count ==1:
#                         grain_id_cnt = int(row[2])
#                         my_matrix = np.zeros((int(row[0]), int(row[1])), dtype=int)
#                         #print(f'x_pos_max={row[0]}, y_pos_max={row[1]}, grain_id_cnt={row[2]}.')
#                     elif line_count ==2:
#                         pass
#                         #print(f'Column names are {", ".join(row)}')
#                     else:
#                         my_matrix[int(row[0]),int(row[1])] = int(row[2])
#                         #print(f'x_pos={row[0]}, y_pos={row[1]}, grain_id={row[2]}.')
#                     line_count += 1
#             print(f'Processed {line_count} lines.')
#     else:
#         print("nieweszłem")
#         grain_id_cnt =-1
#         my_matrix = [[]]
#     return my_matrix, grain_id_cnt
# color_dict = ca.color_dict_rand(2)
#
# print(color_dict)
#
# my_matrix = [[1,1,1,1,1,1,1,1,1,1,1],
# [1,1,1,1,1,1,1,1,1,1,1],
# [1,1,1,1,1,1,1,1,1,1,1],
# [1,1,1,1,1,0,0,1,1,1,1],
# [1,1,1,1,0,0,0,1,1,1,1],
# [2,2,2,0,0,0,2,2,2,2,2],
# [2,2,2,0,0,2,2,2,2,2,2],
# [2,2,2,2,2,2,2,2,2,2,2],
# [2,2,2,2,2,2,2,2,2,2,2],
# [2,2,2,2,2,2,2,2,2,2,2],
# [2,2,2,2,2,2,2,2,2,2,2],
# [2,2,2,2,2,2,2,2,2,2,2],
# ]
#
# # create_PNG_map(my_matrix,color_dict)
#
# export_to_csv(my_matrix,2)
#
# my_matrix_got, cnt = import_from_csv()
#
# print(my_matrix_got)
# print(cnt)