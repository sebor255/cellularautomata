import types

class Employee:

    num_of_emps = 0
    raise_ammount = 1.04

    def __init__(self, first, last, pay):    #runs everytime we create a new instance
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first + "." + last + "@home.com"

        Employee.num_of_emps += 1

    def fullname(self):
        return "{} {}".format(self.first, self.last)

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_ammount)

    @classmethod
    def from_string(cls, emp_str: str):
        first, last, pay = emp_str.split("-")
        return cls(first, last, pay)        #this returns class with this params

    @classmethod
    def set_raise_amount(cls, amount):
        cls.raise_ammount = amount

    @staticmethod       # this is a function which not uses cls nor self
    def is_work_day(day):
        if day.weekday() == 5 or day.weekday() == 6:
            return False
        return True


print(Employee.num_of_emps)
emp_1 = Employee("Seba", "Dres", 100000)
print(Employee.num_of_emps)
emp_2 = Employee.from_string("Gocha-Dres-80000")
print(Employee.num_of_emps)

print(emp_1.pay)
emp_1.apply_raise()
print(emp_1.pay)

emp_1.raise_ammount = 1.05
print(Employee.raise_ammount)

Employee.set_raise_amount(1.08)
print(Employee.raise_ammount)

print(emp_1.raise_ammount)
print(emp_2.raise_ammount)

# print(Employee.__dict__)
# print(emp_1.__dict__)
# print(emp_2.__dict__)

import datetime
my_date = datetime.date(2018,8,11)
print(Employee.is_work_day(my_date))
my_date = datetime.date(2018,8,9)
print(Employee.is_work_day(my_date))

class Developer(Employee):
    raise_ammount = 1.10

    def __init__(self, first, last, pay, prog_lang):
        super().__init__(first, last, pay)
        self.prog_lang = prog_lang

class Manager(Employee):
    def __init__(self, first, last, pay, empl_list=None):
        super().__init__(first, last, pay)
        if empl_list is None:
            self.empl_list = []
        else:
            self.empl_list = empl_list

    def add_emp(self, emp):
        if emp not in self.empl_list:
            self.empl_list.append(emp)

    def rm_emp(self,emp):
        if emp in self.empl_list:
            self.empl_list.remove(emp)

    def print_emp(self):
        for emp in self.empl_list:
            print("---> ", emp.fullname())
        else:
            if len(self.empl_list) == 0:
                print("No employees")

dev_1 = Developer("Marek", "Mostowiak", 10000,"None")
print(dev_1.email)
dev_1.apply_raise()
print(dev_1.pay)
print(dev_1.prog_lang )

man_1 = Manager("Hanka", "Mostowiak", 100)

man_1.print_emp()

man_1.add_emp(emp_1)
man_1.add_emp(dev_1)

man_1.print_emp()
