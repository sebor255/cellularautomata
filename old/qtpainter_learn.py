import sys
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import Qt

from CA_window import CAPainting

class MainWindow(QtWidgets.QMainWindow):

    SIM_WIDTH = 500
    SIM_HIGHT = 500

    def __init__(self):
        super().__init__()
        self.label = QtWidgets.QLabel()
        canvas = QtGui.QPixmap(self.SIM_WIDTH, self.SIM_HIGHT)
        self.label.setPixmap(canvas)
        self.setCentralWidget(self.label)

        self.ca_window = CAPainting(self.label.pixmap())

        self.ca_window.clear_painting()
        # self.ca_window.set_pen_color((200, 30, 100))
        # self.ca_window.draw_point(200, 200)
        # self.ca_window.draw_wide_point(300,300,20)

        # self.timer = QtCore.QTimer()
        # self.timer.timeout.connect(self.update)
        # #self.timer.setInterval(1000)
        # self.timer.start(100)
        # self.ca_window.colors_rand(20)

        self.ca_window.set_painting_hight(200)
        self.ca_window.set_painting_width(200)
        self.ca_window.set_seed_cnt(100)
        self.ca_window.ca_paint_gen_seeds()
        print(self.ca_window.my_painting)
        self.ca_window.ca_paint_calc_next_gen()
        #self.ca_window.ca_paint_play()
    def update(self) -> None:
        print("time's up")




app = QtWidgets.QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec_()
