from old import cellularautomata as ca
from CA_window import CAPainting
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'cellularautomata_ui.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(QtWidgets.QMainWindow):
    METHOD = ca.MOORE_NEIGHBOURHOOD
    SHARP_BOARDER = False
    MATRIX_HIGHT = 250
    MATRIX_WITDH = 200
    SEED_CNT = 5

    def setupUi(self):
        self.setObjectName("MainWindow")
        self.resize(780, 560)

        self.centralwidget = QtWidgets.QWidget()
        self.centralwidget.setObjectName("centralwidget")

        self.simulation_box = QtWidgets.QGroupBox(self.centralwidget)
        self.simulation_box.setGeometry(QtCore.QRect(520, 430, 250, 80))
        self.simulation_box.setObjectName("simulation_box")

        self.forward_btn = QtWidgets.QPushButton(self.simulation_box)
        self.forward_btn.setGeometry(QtCore.QRect(190, 20, 50, 50))
        self.forward_btn.setObjectName("forward_btn")
        self.forward_btn.clicked.connect(self.forv_clicked)

        self.play_btn = QtWidgets.QPushButton(self.simulation_box)
        self.play_btn.setGeometry(QtCore.QRect(70, 20, 50, 50))
        self.play_btn.setObjectName("play_btn")
        self.play_btn.clicked.connect(self.play_clicked)

        self.reverse_btn = QtWidgets.QPushButton(self.simulation_box)
        self.reverse_btn.setGeometry(QtCore.QRect(10, 20, 50, 50))
        self.reverse_btn.setObjectName("reverse_btn")
        self.reverse_btn.clicked.connect(self.rev_clicked)

        self.pause_btn = QtWidgets.QPushButton(self.simulation_box)
        self.pause_btn.setGeometry(QtCore.QRect(130, 20, 50, 50))
        self.pause_btn.setObjectName("pause_btn")
        self.pause_btn.clicked.connect(self.pause_clicked)

        self.generate_btn = QtWidgets.QPushButton(self.centralwidget)
        self.generate_btn.setGeometry(QtCore.QRect(530, 190, 230, 50))
        self.generate_btn.setObjectName("generate_btn")
        self.generate_btn.clicked.connect(self.gen_clicked)

        self.border_type_box = QtWidgets.QGroupBox(self.centralwidget)
        self.border_type_box.setGeometry(QtCore.QRect(520, 10, 81, 71))
        self.border_type_box.setObjectName("border_type_box")

        self.boarder_sharp_rb = QtWidgets.QRadioButton(self.border_type_box)
        self.boarder_sharp_rb.setGeometry(QtCore.QRect(12, 42, 51, 21))
        self.boarder_sharp_rb.setObjectName("boarder_sharp_rb")

        self.boarder_torus_rb = QtWidgets.QRadioButton(self.border_type_box)
        self.boarder_torus_rb.setGeometry(QtCore.QRect(12, 12, 51, 31))
        self.boarder_torus_rb.setObjectName("boarder_torus_rb")

        self.imageCA = QtWidgets.QLabel()
        canvas = QtGui.QPixmap(500, 500)
        self.imageCA.setPixmap(canvas)
        self.setCentralWidget(self.imageCA)
        #
        self.ca_window = CAPainting(self.imageCA.pixmap())
        self.ca_window.clear_painting()


        # self.imageCA = QtWidgets.QLabel(self.centralwidget)
        # self.imageCA.setGeometry(QtCore.QRect(10, 10, 500, 500))
        # self.imageCA.setFrameShape(QtWidgets.QF  rame.WinPanel)
        # #self.imageCA.setScaledContents(True)
        # self.imageCA.setObjectName("imageCA")
        # canvas = QtGui.QPixmap(500, 500)
        # self.imageCA.setPixmap(canvas)
        # #self.setCentralWidget(self.imageCA)
        #
        # self.ca_window = CAPainting(self.imageCA.pixmap())
        # self.ca_window.clear_painting()

        self.method_cb = QtWidgets.QComboBox(self.centralwidget)
        self.method_cb.setGeometry(QtCore.QRect(610, 30, 160, 20))
        self.method_cb.setObjectName("method_cb")
        self.method_cb.addItem("")
        self.method_cb.addItem("")
        self.method_cb.currentIndexChanged.connect(self.combo_choose)

        self.label_nei = QtWidgets.QLabel(self.centralwidget)
        self.label_nei.setGeometry(QtCore.QRect(610, 10, 90, 15))
        self.label_nei.setObjectName("label_nei")

        self.width_le = QtWidgets.QLineEdit(self.centralwidget)
        self.width_le.setGeometry(QtCore.QRect(530, 120, 100, 20))
        self.width_le.setObjectName("width_le")
        self.width_le.editingFinished.connect(self.width_enter)

        self.label_width = QtWidgets.QLabel(self.centralwidget)
        self.label_width.setGeometry(QtCore.QRect(530, 100, 60, 15))
        self.label_width.setObjectName("label_width")

        self.label_hight = QtWidgets.QLabel(self.centralwidget)
        self.label_hight.setGeometry(QtCore.QRect(650, 100, 47, 13))
        self.label_hight.setObjectName("label_hight")

        self.hight_le = QtWidgets.QLineEdit(self.centralwidget)
        self.hight_le.setGeometry(QtCore.QRect(650, 120, 113, 20))
        self.hight_le.setObjectName("hight_le")
        self.hight_le.editingFinished.connect(self.hight_enter)

        self.label_seed = QtWidgets.QLabel(self.centralwidget)
        self.label_seed.setGeometry(QtCore.QRect(650, 50, 71, 16))
        self.label_seed.setObjectName("label_seed")

        self.seed_le = QtWidgets.QLineEdit(self.centralwidget)
        self.seed_le.setGeometry(QtCore.QRect(650, 70, 113, 20))
        self.seed_le.setObjectName("seed_le")
        self.seed_le.editingFinished.connect(self.seed_enter)

        self.setCentralWidget(self.centralwidget)

        self.menubar = QtWidgets.QMenuBar()
        self.menubar.setGeometry(QtCore.QRect(0, 0, 783, 21))
        self.menubar.setObjectName("menubar")

        self.setMenuBar(self.menubar)

        self.statusbar = QtWidgets.QStatusBar()
        self.statusbar.setObjectName("statusbar")

        self.setStatusBar(self.statusbar)

        self.ui_update()

        self.retranslateUi()
        #QtCore.QMetaObject.connectSlotsByName()

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "Cellular Automata"))
        self.simulation_box.setTitle(_translate("MainWindow", "Simulation"))
        self.forward_btn.setText(_translate("MainWindow", "FORV"))
        self.play_btn.setText(_translate("MainWindow", "PLAY"))
        self.reverse_btn.setText(_translate("MainWindow", "REV"))
        self.pause_btn.setText(_translate("MainWindow", "PAUSE"))
        self.border_type_box.setTitle(_translate("MainWindow", "Border Type"))
        self.boarder_sharp_rb.setText(_translate("MainWindow", "Sharp"))
        self.boarder_torus_rb.setText(_translate("MainWindow", "Torus"))
        self.imageCA.setText(_translate("MainWindow", "Cellula Automata result"))
        self.generate_btn.setText(_translate("MainWindow", "Generate seeds"))
        self.method_cb.setItemText(0, _translate("MainWindow", "von Neumann"))
        self.method_cb.setItemText(1, _translate("MainWindow", "Moore"))
        self.label_nei.setText(_translate("MainWindow", "Neighbourhood"))
        self.label_width.setText(_translate("MainWindow", "Width:"))
        self.label_hight.setText(_translate("MainWindow", "Hight:"))
        self.label_seed.setText(_translate("MainWindow", "Seed count:"))

    def play_clicked(self):
        print("PLAY clicked")
        self.ca_window.ca_paint_play()

    def rev_clicked(self):
        print("rev_clicked DOES nothing")
        self.ui_update()

    def pause_clicked(self):
        print("pause_clicked DOES nothing")
        self.ui_update()

    def forv_clicked(self):
        print("FORV clicked")
        self.ca_window.ca_paint_calc_next_gen()

    def gen_clicked(self):
        print("gen clicked")
        #self.ca_window.set_pen_width(1)
        self.ca_window.ca_paint_gen_seeds()

        #self.ui_update()

    def seed_enter(self):
        print("seed enter")
        self.ca_window.set_seed_cnt( int(self.seed_le.text()) )
        print(self.ca_window.SEED_CNT)
        #self.ui_update()

    def width_enter(self):
        print("width enter")
        self.ca_window.set_painting_width( int(self.width_le.text()) )
        print(self.ca_window.MAX_WIDTH)
        #self.ui_update()

    def hight_enter(self):
        print("hight enter")
        self.ca_window.set_painting_hight(int(self.hight_le.text()))
        print(self.ca_window.MAX_HIGHT)
        #self.ui_update()

    def combo_choose(self):
        print("combo_choose enter")
        temp = self.method_cb.currentText()
        if "Moore" ==  temp:
            self.ca_window.set_neighbourhood(ca.MOORE_NEIGHBOURHOOD)
            print("Moore")
        elif "von Neumann" == temp:
            self.ca_window.set_neighbourhood(ca.VONNEUMANN_NEIGHBOURHOOD)
            print("von Neumann")

    # def update_image(self):
    #     self.imageCA.setPixmap(QtGui.QPixmap(path_snap_actual))

    def ui_update(self):
        ############### INIT VALUES ####################
        self.width_le.setText(str(self.MATRIX_WITDH))
        self.hight_le.setText(str(self.MATRIX_HIGHT))
        self.seed_le.setText(str(self.SEED_CNT))
        self.imageCA.show()
        ################################################

if __name__ == "__main__":
    import sys
    import os

    ############################





    ############################
    path = os.getcwd() + "\\"
    sub_dir_images = "snapshot\\"
    snapshot_actual= "actual.png"
    path_snap_actual = path + sub_dir_images + snapshot_actual
    #path_snapshot2 = path + sub_dir_images + snapshot_filename2

    #my_matrix = np.zeros((MATRIX_WITDH, MATRIX_HEIGHT), dtype=int)
    #ca.matrix_random_init(my_matrix, 30)
    # print(my_matrix)

    # ca.create_PNG_map(my_matrix, snapshot_actual, color_dict)
    # ca.grain_next_gen(my_matrix, METHOD, SHARP_BOARDER)
    # print(my_matrix)

    # app = QtWidgets.QApplication(sys.argv)
    # MainWindow = QtWidgets.QMainWindow()
    # ui = Ui_MainWindow()
    # ui.setupUi(MainWindow)
    # MainWindow.show()
    # sys.exit(app.exec_())

    app = QtWidgets.QApplication(sys.argv)
    window = Ui_MainWindow()
    window.setupUi()
    window.show()
    app.exec_()

## MAIN ##
#
# for x in range(80):                     #TODO: Make steps to get final result
#     print(f"next iter: {x}")
#     grain_next_gen(my_matrix, METHOD,SHARP_BOARDER)
#     create_PNG_map(my_matrix, path + sub_dir_images + "final.png", color_dict)
#


#print(grain_ID_distrib([2, 0, 0, 0]))

