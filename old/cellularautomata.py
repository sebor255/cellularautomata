import numpy as np
from typing import *
import random
from PIL import Image
import copy



MOORE_NEIGHBOURHOOD = ((-1,-1), (0,-1),  (+1,-1),
                       (-1,0),           (+1,0),
                       (-1,+1), (0,+1),  (+1,+1))

VONNEUMANN_NEIGHBOURHOOD = (         (0,-1),
                            (-1,0),           (+1,0),
                                     (0,+1)         )

def matrix_random_init(my_matrix: List[List], number: int) -> List[List]:
    initiated_pos = []
    columns = len(my_matrix)
    rows = len(my_matrix[0])
    id_number = 0
    while(id_number<number):
        temp_col = random.randrange(0,columns)
        temp_row = random.randrange(0,rows)
        if [temp_col,temp_row] not in initiated_pos:
            id_number += next_ID
            initiated_pos.append([temp_col,temp_row])
            my_matrix[temp_col][temp_row] = id_number
    return initiated_pos


#function looks
def grain_ID_distrib(id_list: List) -> int:
    NO_CELL = 0
    set_id = 0
    most_frequent = 0
    unique_vals = set(id_list)
    if len(id_list) > 0:
        for val in unique_vals:
            if val != NO_CELL:
                frequence = id_list.count(val)
                #print(frequence)
                if frequence == most_frequent:
                    set_id = random.choice([set_id, val])         #if there is the same frequence of 2 IDs choice one
                elif frequence > most_frequent:
                    most_frequent = frequence
                    set_id = val                                #set the most frequent ID
    return set_id



def grain_next_gen(my_matrix: List[List], nei_method: Tuple[Tuple], sharp_border: int) -> int:
    NO_CELL = 0
    old_matrix = copy.deepcopy(my_matrix)
    len_hight = len(my_matrix[0])
    len_width = len(my_matrix)

    empty_cnt = 0
    ## Warunki brzegowe

    ## Algorytm next_gen
    for grain_pos_hight in range(len_hight):
        for grain_pos_width in range(len_width):
            #print(f"PRAWILNE COORDY {grain_pos_width}, {grain_pos_hight}  ")
            analyzed_nei = list()
            if NO_CELL == old_matrix[grain_pos_width,grain_pos_hight]:
                empty_cnt +=1
                for nei_cell in nei_method:                                                                             #for every single neibourhood cell
                    nei_width = (grain_pos_width + nei_cell[0])
                    nei_hight = (grain_pos_hight + nei_cell[1])
                    flag_sb_in_range = 1
                    if sharp_border !=0:
                        if nei_width < 0 or nei_width >= len_width or nei_hight < 0 or nei_hight >= len_hight:
                            flag_sb_in_range = 0
                    else:                                                                                               #acts like torus when analyzed cell is out of the matrix
                        nei_width = nei_width % len_width
                        nei_hight = nei_hight % len_hight

                    if flag_sb_in_range == 0:
                        analyzed_nei.append(0)
                    else:
                        analyzed_nei.append(old_matrix[nei_width, nei_hight])
                #     print(f"NEI:  {nei_width}, {nei_hight}  ")
                # print(analyzed_nei)
                my_matrix[grain_pos_width, grain_pos_hight] = grain_ID_distrib(analyzed_nei)                            #set most frequent ID
    return empty_cnt




def color_distrib_rand(value_of_cell, colours: Dict) -> [Tuple, Dict]:
    if value_of_cell in colours.keys():
        return colours.get(value_of_cell), colours
    else:
        colours.update( {value_of_cell: (random.randint(0,255), random.randint(0,255), random.randint(0,255))})
        return colours.get(value_of_cell), colours

def create_PNG_map(my_matrix: List[List], path: str, color_dict: Dict):
    temp_width = len(my_matrix)
    temp_hight = len(my_matrix[0])
    img = Image.new('RGB', (temp_width, temp_hight), "black")
    pixels = img.load()

    for j in range(0,temp_hight):
        for i in range(0,temp_width):
            new_rgb, color_dict = color_distrib_rand(my_matrix[i][j], color_dict)
            pixels[i,j] = new_rgb
            #print(new_RGB)
    #img.show()
    img.save(path)
    #print(color_dict)
    return img

## MAIN ##
import os
# ############################
# MATRIX_HEIGHT=250
# MATRIX_WITDH=200
next_ID=1
# SHARP_BOARDER = True
# METHOD = MOORE_NEIGHBOURHOOD

color_dict = dict()
color_dict.update({0: (255, 255, 255)})

############################
path = os.getcwd() + "\\"
sub_dir_images = "snapshot\\"
# snapshot_filename1 = "snapshot1.png"
# snapshot_filename2 = "snapshot2.png"
# path_snapshot1 = path + sub_dir_images + snapshot_filename1
# path_snapshot2 = path + sub_dir_images + snapshot_filename2

snapshot_actual= "actual.png"
path_snap_actual = path + sub_dir_images + snapshot_actual
#print(my_matrix)

# my_matrix = np.zeros((MATRIX_WITDH,MATRIX_HEIGHT),dtype=int)
# matrix_random_init(my_matrix,30)
#print(my_matrix)

# create_PNG_map(my_matrix, path_snapshot1, color_dict)
# grain_next_gen(my_matrix,METHOD,SHARP_BOARDER)
# #print(my_matrix)
# create_PNG_map(my_matrix, path_snapshot2, color_dict)

# for x in range(80):                     #TODO: Make steps to get final result
#     print(f"next iter: {x}")
#     grain_next_gen(my_matrix, METHOD,SHARP_BOARDER)
#     create_PNG_map(my_matrix, path + sub_dir_images + "final.png", color_dict)
#


#print(grain_ID_distrib([2, 0, 0, 0]))

