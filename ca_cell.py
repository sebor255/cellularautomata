# Cell has ID of grain anf phase
# It should know also self neigborhood? or not


class Cell:
    def __init__(self):
        self.grain_id = 0
        self.phase_id = 0
        self.incl_id = 0

    def set_grain_id(self, grain_id):
        self.grain_id = grain_id

    def get_grain_id(self):
        return self.grain_id

    def set_phase_id(self, phase_id):
        self.phase_id = phase_id

    def get_phase_id(self):
        return self.phase_id

    def set_inclusion_id(self, incl_id):
        self.incl_id = incl_id

    def get_inclusion_id(self):
        return self.incl_id






