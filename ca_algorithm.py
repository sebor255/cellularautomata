import random
from typing import List, Tuple, Dict
import copy
import numpy as np

MOORE_NEIGHBOURHOOD = ((-1,-1), (0,-1),  (+1,-1),
                       (-1,0),           (+1,0),
                       (-1,+1), (0,+1),  (+1,+1))

VONNEUMANN_NEIGHBOURHOOD = (         (0,-1),
                            (-1,0),           (+1,0),
                                     (0,+1)         )

def color_dict_rand(colors_cnt: int) -> Dict:
    colors = dict()
    colors.update({0: (255, 255, 255)})                         #default color has to be always white
    for x in range(colors_cnt):
        colors.update({(x+1): (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))})
    return colors

def color_dict_incl_rand(colors_cnt: int) -> Dict:
    colors_added = 2
    colors = dict()
    colors.update({0: (255, 255, 255)})                         #default color has to be always white
    colors.update({1: (0,0,0)})                                 #inclusion color
    for x in range(colors_cnt):
        colors.update({(x+colors_added): (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))})
    return colors

def color_distrib(value_of_cell, colours: Dict) -> Tuple:
    #print("Entered: color_distrib")
    return colours.get(value_of_cell, None)

def matrix_random_init(my_matrix: List[List], number: int) -> List[List]:
    #print("Enetered: matrix_random_init")
    initiated_pos = []
    columns = len(my_matrix)
    rows = len(my_matrix[0])
    id_number = 0
    while(id_number<number):
        temp_col = random.randrange(0,columns)
        temp_row = random.randrange(0,rows)
        if [temp_col,temp_row] not in initiated_pos:
            id_number += 1
            initiated_pos.append([temp_col,temp_row])
            my_matrix[temp_col][temp_row] = id_number
    #print("--->matrix_random_init: initiation done")
    return initiated_pos

def grain_ID_distrib(id_list: List) -> int:
    NO_CELL = 0
    set_id = 0
    most_frequent = 0
    unique_vals = set(id_list)
    if len(id_list) > 0:
        for val in unique_vals:
            if val != NO_CELL:
                frequence = id_list.count(val)
                #print(frequence)
                if frequence == most_frequent:
                    set_id = random.choice([set_id, val])           #if there is the same frequence of 2 IDs choice one
                elif frequence > most_frequent:
                    most_frequent = frequence
                    set_id = val                                    #set the most frequent ID
    return set_id

def grain_next_gen(my_matrix: List[List], nei_method: Tuple[Tuple], sharp_border: bool) -> int:
    NO_CELL = 0
    old_matrix = copy.deepcopy(my_matrix)
    len_hight = len(my_matrix[0])
    len_width = len(my_matrix)

    empty_cnt = 0
    #print("sharp_border is: ",sharp_border)
    ## Algorytm next_gen
    for grain_pos_hight in range(len_hight):
        for grain_pos_width in range(len_width):
            #print(f"PRAWILNE COORDY {grain_pos_width}, {grain_pos_hight}  ")
            analyzed_nei = list()
            if NO_CELL == old_matrix[grain_pos_width, grain_pos_hight]:
                empty_cnt +=1
                for nei_cell in nei_method:                                                                             #for every single neibourhood cell
                    nei_width = (grain_pos_width + nei_cell[0])
                    nei_hight = (grain_pos_hight + nei_cell[1])
                    flag_sb_in_range = 1
                    if sharp_border is True:
                        if nei_width < 0 or nei_width >= len_width or nei_hight < 0 or nei_hight >= len_hight:
                            flag_sb_in_range = 0
                    else:                                                                                               #acts like torus when analyzed cell is out of the matrix
                        nei_width = nei_width % len_width
                        nei_hight = nei_hight % len_hight
                        #print("Nei width: ",nei_width, "Nei hight: ", nei_hight)

                    if flag_sb_in_range == 0:
                        analyzed_nei.append(0)
                    else:
                        analyzed_nei.append(old_matrix[nei_width, nei_hight])
                #     print(f"NEI:  {nei_width}, {nei_hight}  ")
                # print(analyzed_nei)
                my_matrix[grain_pos_width, grain_pos_hight] = grain_ID_distrib(analyzed_nei)                            #set most frequent ID
    return empty_cnt

def matrix_init(width: int, hight: int):
    return np.zeros((width, hight), dtype=int)

