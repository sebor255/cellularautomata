import sys, traceback
import time
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import ca_io_operations as caio
from ca_sim import CASim


from CA_window2 import CAPainting


class WorkerSignals(QObject):
    '''
    Defines the signals available from a running worker thread.

    Supported signals are:

    finished
        No data

    error
        `tuple` (exctype, value, traceback.format_exc() )

    result
        `object` data returned from processing, anything

    progress
        `int` indicating % progress

    '''
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)


class Worker(QRunnable):
    '''
    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    '''

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()

        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()

        # Add the callback to our kwargs
        #self.kwargs['progress_callback'] = self.signals.progress

    @pyqtSlot()
    def run(self):
        '''
        Initialise the runner function with passed args, kwargs.
        '''

        # Retrieve args/kwargs here; and fire processing using them
        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)  # Return the result of the processing
        finally:
            self.signals.finished.emit()  # Done


class MainWindow(QtWidgets.QMainWindow):

    MIN_RADIUS = 0.5
    MAX_RADIUS = 300

    ca_sim_width = 200
    ca_sim_hight = 200
    ca_sim_seeds = 100
    ca_sim_inclusions = 0
    min_radius = 0.5
    max_radius = 40
    sharp_border = True
    ca_sim_change_probab = 50
    # incl_id
    # phase_id

    to_fill_cnt = 1
    flag_play = False

    trig_next_gen = pyqtSignal()

    ca_sim = CASim(ca_sim_width, ca_sim_hight, ca_sim_seeds, ca_sim_inclusions, 1, 1, min_radius, max_radius,
                   sharp_border)

    def __init__(self):
        super().__init__()

        self.setObjectName("MainWindow")
        self.resize(780, 560)
        self.centralwidget = QtWidgets.QWidget()
        self.centralwidget.setObjectName("centralwidget")

        self.simulation_box = QtWidgets.QGroupBox(self.centralwidget)
        self.simulation_box.setGeometry(QtCore.QRect(520, 430, 191, 80))
        self.simulation_box.setObjectName("simulation_box")

        self.forward_btn = QtWidgets.QPushButton(self.simulation_box)
        self.forward_btn.setGeometry(QtCore.QRect(130, 20, 50, 50))
        self.forward_btn.setObjectName("forward_btn")
        self.forward_btn.clicked.connect(self.forv_clicked)

        self.play_btn = QtWidgets.QPushButton(self.simulation_box)
        self.play_btn.setGeometry(QtCore.QRect(10, 20, 50, 50))
        self.play_btn.setObjectName("play_btn")
        self.play_btn.clicked.connect(self.play_clicked)

        self.pause_btn = QtWidgets.QPushButton(self.simulation_box)
        self.pause_btn.setGeometry(QtCore.QRect(70, 20, 50, 50))
        self.pause_btn.setObjectName("pause_btn")
        self.pause_btn.clicked.connect(self.pause_clicked)

        self.generate_btn = QtWidgets.QPushButton(self.centralwidget)
        self.generate_btn.setGeometry(QtCore.QRect(520, 370, 251, 50))
        self.generate_btn.setObjectName("generate_btn")
        self.generate_btn.clicked.connect(self.gen_clicked)

        self.gen_substr_btn = QtWidgets.QPushButton(self.centralwidget)
        self.gen_substr_btn.setGeometry(QtCore.QRect(520, 310, 121, 50))
        self.gen_substr_btn.setObjectName("gen_substr_btn")
        self.gen_substr_btn.clicked.connect(self.gen_substr_clicked)

        self.gen_phase_btn = QtWidgets.QPushButton(self.centralwidget)
        self.gen_phase_btn.setGeometry(QtCore.QRect(650, 310, 121, 50))
        self.gen_phase_btn.setObjectName("gen_phase_btn")
        self.gen_phase_btn.clicked.connect(self.gen_phase_clicked)

        self.border_type_box = QtWidgets.QGroupBox(self.centralwidget)
        self.border_type_box.setGeometry(QtCore.QRect(520, 10, 81, 71))
        self.border_type_box.setObjectName("border_type_box")

        self.boarder_sharp_rb = QtWidgets.QRadioButton(self.border_type_box)
        self.boarder_sharp_rb.setGeometry(QtCore.QRect(12, 42, 51, 21))
        self.boarder_sharp_rb.setObjectName("boarder_sharp_rb")
        self.boarder_sharp_rb.clicked.connect(self.method_set_sharp)
        self.boarder_sharp_rb.setChecked(True)

        self.boarder_torus_rb = QtWidgets.QRadioButton(self.border_type_box)
        self.boarder_torus_rb.setGeometry(QtCore.QRect(12, 12, 51, 31))
        self.boarder_torus_rb.setObjectName("boarder_torus_rb")
        self.boarder_torus_rb.clicked.connect(self.method_set_torus)
        self.boarder_torus_rb.setChecked(False)

        self.imageCA = QtWidgets.QLabel(self.centralwidget)
        self.imageCA.setGeometry(QtCore.QRect(10, 10, 500, 500))
        self.imageCA.setFrameShape(QtWidgets.QFrame.WinPanel)
        canvas = QtGui.QPixmap(500, 500)
        self.imageCA.setPixmap(canvas)
        self.imageCA.setObjectName("imageCA")

        self.ca_window = CAPainting(self.imageCA.pixmap())
        self.ca_window.clear_painting()

        self.method_cb = QtWidgets.QComboBox(self.centralwidget)
        self.method_cb.setGeometry(QtCore.QRect(610, 30, 160, 20))
        self.method_cb.setObjectName("method_cb")
        self.method_cb.addItem("")
        self.method_cb.addItem("")
        self.method_cb.currentIndexChanged.connect(self.combo_choose)

        self.label_nei = QtWidgets.QLabel(self.centralwidget)
        self.label_nei.setGeometry(QtCore.QRect(610, 10, 90, 15))
        self.label_nei.setObjectName("label_nei")

        self.width_le = QtWidgets.QLineEdit(self.centralwidget)
        self.width_le.setGeometry(QtCore.QRect(530, 120, 100, 20))
        self.width_le.setObjectName("width_le")
        self.width_le.editingFinished.connect(self.width_enter)

        self.label_width = QtWidgets.QLabel(self.centralwidget)
        self.label_width.setGeometry(QtCore.QRect(530, 100, 60, 15))
        self.label_width.setObjectName("label_width")

        self.label_hight = QtWidgets.QLabel(self.centralwidget)
        self.label_hight.setGeometry(QtCore.QRect(650, 100, 47, 13))
        self.label_hight.setObjectName("label_hight")

        self.hight_le = QtWidgets.QLineEdit(self.centralwidget)
        self.hight_le.setGeometry(QtCore.QRect(650, 120, 113, 20))
        self.hight_le.setObjectName("hight_le")
        self.hight_le.editingFinished.connect(self.hight_enter)

        self.label_seed = QtWidgets.QLabel(self.centralwidget)
        self.label_seed.setGeometry(QtCore.QRect(650, 50, 71, 16))
        self.label_seed.setObjectName("label_seed")

        self.seed_le = QtWidgets.QLineEdit(self.centralwidget)
        self.seed_le.setGeometry(QtCore.QRect(650, 70, 113, 20))
        self.seed_le.setObjectName("seed_le")
        self.seed_le.editingFinished.connect(self.seed_enter)

        self.min_rad_le = QtWidgets.QLineEdit(self.centralwidget)
        self.min_rad_le.setGeometry(QtCore.QRect(650, 170, 100, 20))
        self.min_rad_le.setObjectName("min_rad_le")
        self.min_rad_le.editingFinished.connect(self.incl_min_rad_enter)

        self.label_min_rad = QtWidgets.QLabel(self.centralwidget)
        self.label_min_rad.setGeometry(QtCore.QRect(650, 150, 60, 15))
        self.label_min_rad.setObjectName("label_min_rad")

        self.max_rad_le = QtWidgets.QLineEdit(self.centralwidget)
        self.max_rad_le.setGeometry(QtCore.QRect(650, 220, 100, 20))
        self.max_rad_le.setObjectName("max_rad_le")
        self.max_rad_le.editingFinished.connect(self.incl_max_rad_enter)

        self.label_max_rad = QtWidgets.QLabel(self.centralwidget)
        self.label_max_rad.setGeometry(QtCore.QRect(650, 200, 60, 15))
        self.label_max_rad.setObjectName("label_max_rad")

        self.incl_cnt_le = QtWidgets.QLineEdit(self.centralwidget)
        self.incl_cnt_le.setGeometry(QtCore.QRect(530, 170, 100, 20))
        self.incl_cnt_le.setObjectName("incl_cnt_le")
        self.incl_cnt_le.editingFinished.connect(self.incl_cnt_enter)

        self.label_incl_cnt = QtWidgets.QLabel(self.centralwidget)
        self.label_incl_cnt.setGeometry(QtCore.QRect(530, 150, 81, 16))
        self.label_incl_cnt.setObjectName("label_incl_cnt")

        self.gbc_chkbox = QtWidgets.QCheckBox(self.centralwidget)
        self.gbc_chkbox.setGeometry(QtCore.QRect(530, 250, 70, 17))
        self.gbc_chkbox.setObjectName("gbc_chkbox")
        self.gbc_chkbox.clicked.connect(self.gbc_clicked)

        self.change_probab_le = QtWidgets.QLineEdit(self.centralwidget)
        self.change_probab_le.setGeometry(QtCore.QRect(530, 220, 101, 20))
        self.change_probab_le.setObjectName("change_probab_le")
        self.change_probab_le.editingFinished.connect(self.change_probab_enter)

        self.label_change_probab = QtWidgets.QLabel(self.centralwidget)
        self.label_change_probab.setGeometry(QtCore.QRect(530, 200, 121, 16))
        self.label_change_probab.setObjectName("label_change_probab")

        self.calc_boundaries_btn = QtWidgets.QPushButton(self.centralwidget)
        self.calc_boundaries_btn.setGeometry(QtCore.QRect(710, 449, 61, 51))
        self.calc_boundaries_btn.setObjectName("calc_boundaries_btn")
        self.calc_boundaries_btn.clicked.connect(self.calc_boundaries_clicked)

        self.label_mean_grain_size = QtWidgets.QLabel(self.centralwidget)
        self.label_mean_grain_size.setGeometry(QtCore.QRect(530, 280, 121, 16))
        self.label_mean_grain_size.setObjectName("label_mean_grain_size")

        self.label_boundary_len = QtWidgets.QLabel(self.centralwidget)
        self.label_boundary_len.setGeometry(QtCore.QRect(650, 280, 121, 16))
        self.label_boundary_len.setObjectName("label_boundary_len")
        ########################################################
        self.setCentralWidget(self.centralwidget)
        ########################################################
        self.menubar = QtWidgets.QMenuBar()
        self.menubar.setGeometry(QtCore.QRect(0, 0, 783, 21))
        self.menubar.setObjectName("menubar")

        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setToolTipsVisible(False)
        self.menuFile.setObjectName("menuFile")

        self.setMenuBar(self.menubar)

        self.statusbar = QtWidgets.QStatusBar()
        self.statusbar.setObjectName("statusbar")

        self.setStatusBar(self.statusbar)

        self.actionImport_CSV = QtWidgets.QAction()
        self.actionImport_CSV.setObjectName("actionImport_CSV")
        self.actionImport_CSV.triggered.connect(self.import_csv)

        self.actionExport_CSV = QtWidgets.QAction()
        self.actionExport_CSV.setObjectName("actionExport_CSV")
        self.actionExport_CSV.triggered.connect(self.export_csv)

        self.actionSave_image = QtWidgets.QAction()
        self.actionSave_image.setObjectName("actionSave_image")
        self.actionSave_image.triggered.connect(self.save_image)

        self.menuFile.addAction(self.actionExport_CSV)
        self.menuFile.addAction(self.actionSave_image)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionImport_CSV)
        self.menubar.addAction(self.menuFile.menuAction())

        self.ui_update()

        self.retranslateUi()

        ################## TIMERS and SIGNALS ########################
        self.trig_next_gen.connect(self.next_gen)

        self.timer1 = QtCore.QTimer()
        self.__init_timer1()

        ################## THREADS ######################
        self.threadpool = QThreadPool()
        print("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())
        self.worker = None
        # self.threadpool.start(self.worker)
        self.timer1.start(1000)

    def timer1_up(self) -> None:
        #print("timer 1 time's up")
        self.imageCA.update()
        self.timer1.start(50)

    def next_gen(self):
        print("next_gen")

    def __init_timer1(self):
        self.timer1.timeout.connect(self.timer1_up)
        self.timer1.setSingleShot(1)

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "Cellular Automata"))
        self.simulation_box.setTitle(_translate("MainWindow", "Simulation"))
        self.forward_btn.setText(_translate("MainWindow", "FORV"))
        self.play_btn.setText(_translate("MainWindow", "PLAY"))
        self.pause_btn.setText(_translate("MainWindow", "PAUSE"))
        self.border_type_box.setTitle(_translate("MainWindow", "Border Type"))
        self.boarder_sharp_rb.setText(_translate("MainWindow", "Sharp"))
        self.boarder_torus_rb.setText(_translate("MainWindow", "Torus"))
        self.generate_btn.setText(_translate("MainWindow", "Generate seeds"))
        self.gen_substr_btn.setText(_translate("MainWindow", "Gen substructure"))
        self.gen_phase_btn.setText(_translate("MainWindow", "Gen new phase"))
        self.method_cb.setItemText(0, _translate("MainWindow", "von Neumann"))
        self.method_cb.setItemText(1, _translate("MainWindow", "Moore"))
        self.label_nei.setText(_translate("MainWindow", "Neighbourhood"))
        self.label_width.setText(_translate("MainWindow", "Width:"))
        self.label_hight.setText(_translate("MainWindow", "Hight:"))
        self.label_seed.setText(_translate("MainWindow", "Seed count:"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.actionImport_CSV.setText(_translate("MainWindow", "Import CSV"))
        self.actionExport_CSV.setText(_translate("MainWindow", "Export CSV"))
        self.actionSave_image.setText(_translate("MainWindow", "Save image"))
        self.label_min_rad.setText(_translate("MainWindow", "Min radius:"))
        self.label_max_rad.setText(_translate("MainWindow", "Max radius:"))
        self.label_incl_cnt.setText(_translate("MainWindow", "Inclusion count:"))
        self.gbc_chkbox.setText(_translate("MainWindow", "GBC"))
        self.label_change_probab.setText(_translate("MainWindow", "Probability of change:"))
        self.calc_boundaries_btn.setText(_translate("MainWindow", "Calculate\n"
                                                                  "boundaries"))
        self.label_mean_grain_size.setText(_translate("MainWindow", "Mean grain size: "))
        self.label_boundary_len.setText(_translate("MainWindow", "Boundary len: "))


    def ui_update(self):
        # ############## INIT VALUES ####################
        if self.ca_sim.seed_cnt_total != 0:
            mean_grain_size = self.ca_sim.get_painting_hight()*self.ca_sim.get_painting_width()/self.ca_sim.seed_cnt_total
        else:
            mean_grain_size = self.ca_sim.get_painting_hight() * self.ca_sim.get_painting_width() / self.ca_sim.seed_cnt
        print("mean_grain_size", mean_grain_size)
        self.width_le.setText(str(self.ca_sim.get_painting_width()))
        self.hight_le.setText(str(self.ca_sim.get_painting_hight()))
        self.seed_le.setText(str(self.ca_sim.get_seed_cnt()))
        self.incl_cnt_le.setText(str(self.ca_sim.get_inclusion_cnt()))
        self.min_rad_le.setText(str(self.ca_sim.get_inclusion_min_radius()))
        self.max_rad_le.setText(str(self.ca_sim.get_inclusion_max_radius()))
        self.change_probab_le.setText(str(self.ca_sim.get_change_probability()))
        self.method_cb.setCurrentText(str(self.ca_sim.get_neighborhood_method()))
        self.label_mean_grain_size.setText(str("Mean grain size: {}".format(mean_grain_size)))
        self.label_boundary_len.setText(str("Boundary len: {}".format(self.ca_sim.get_boundary_len())))
        ################################################

    def play_clicked(self):
        print("play_clicked")
        self.worker = Worker(self.forv_clicked_th)
        self.worker.signals.finished.connect(self.worker_finished)
        self.threadpool.start(self.worker)
        self.flag_play = True

    def rev_clicked(self):
        print("rev_clicked")
        pass

    def worker_finished(self):
        if self.flag_play is True:
            if self.ca_sim.get_empty_cell_value() > 0:
                self.worker = Worker(self.forv_clicked_th)
                self.worker.signals.finished.connect(self.worker_finished)
                self.threadpool.start(self.worker)

    def pause_clicked(self):
        print("pause_clicked")
        self.flag_play = False

    def forv_clicked(self):
        print("forv_clicked")
        self.worker = Worker(self.forv_clicked_th)
        self.threadpool.start(self.worker)

    def forv_clicked_th(self):
        self.ca_sim.next_gen()
        self.ca_window.ca_paint_draw(self.ca_sim)


    def gen_clicked(self):
        print("gen clicked")
        self.worker = Worker(self.gen_clicked_th)
        self.threadpool.start(self.worker)

    def gen_clicked_th(self):
        self.ca_window.clear_painting()
        self.ca_sim = CASim(self.ca_sim_width, self.ca_sim_hight, self.ca_sim_seeds, self.ca_sim_inclusions, 1, 1,
                            self.min_radius, self.max_radius, self.sharp_border)
        self.ca_sim.rand_inclusions()
        self.ca_sim.rand_seeds()
        self.ca_sim.set_change_probability(self.ca_sim_change_probab)
        if self.gbc_chkbox.isChecked():
            self.ca_sim.set_gbc(True)
        self.ca_window.ca_paint_draw(self.ca_sim)

    def draw_image(self):
        print("Draw image")
        self.ca_window.ca_paint_draw(self.ca_sim)

    def seed_enter(self):
        print("seed enter")
        temp = int(self.seed_le.text())
        if temp < 1:
            temp = 1
        self.ca_sim_seeds = temp
        self.ca_sim.set_seed_cnt(self.ca_sim_seeds)
        print(self.ca_sim.get_seed_cnt())
        self.ui_update()

    def width_enter(self):
        print("width enter")
        temp = int(self.width_le.text())
        if temp > 500:
            temp = 500
        if temp < 10:
            temp = 10
        self.ca_sim_width = temp
        print(self.ca_sim_width)
        self.ca_sim.set_painting_width(temp)
        print(self.ca_sim.get_painting_width())
        self.ui_update()

    def hight_enter(self):
        print("hight enter")
        temp = int(self.hight_le.text())
        if temp>500:
            temp = 500
        if temp<10:
            temp = 10
        self.ca_sim_hight = temp
        print(self.ca_sim_hight)
        self.ca_sim.set_painting_hight(temp)
        print(self.ca_sim.get_painting_hight())
        self.ui_update()

    def combo_choose(self):
        print("combo_choose enter")
        nei = self.method_cb.currentText()
        self.ca_sim.set_neighborhood_method(nei)

    def method_set_sharp(self):
        print("method_set_sharp enter")
        self.ca_sim.set_sharp_border(True)
        self.boarder_sharp_rb.setChecked(True)
        self.boarder_torus_rb.setChecked(False)

    def method_set_torus(self):
        print("method_set_torus enter")
        self.ca_sim.set_sharp_border(False)
        self.boarder_sharp_rb.setChecked(False)
        self.boarder_torus_rb.setChecked(True)

    def import_csv(self):
        print("import_csv clicked")
        self.ca_sim = caio.import_from_csv()

        self.ca_sim_width = self.ca_sim.get_painting_width()
        self.ca_sim_hight = self.ca_sim.get_painting_hight()
        self.ca_sim_seeds = self.ca_sim.get_seed_cnt()
        self.ca_sim_inclusions = self.ca_sim.get_inclusion_cnt()

        # setting gbc is
        self.ca_sim.set_change_probability(self.ca_sim_change_probab)
        if self.gbc_chkbox.isChecked():
            self.ca_sim.set_gbc(True)

        self.ui_update()

        self.worker = Worker(self.draw_image)
        self.threadpool.start(self.worker)

    def export_csv(self):
        print("export_csv clicked")
        if self.ca_sim.get_painting_hight() > 0:
            caio.export_sim_to_csv(self.ca_sim)
        else:
            print("The image hasn't been generated")

    def save_image(self):
        pass
        # print("save_image clicked")
        # if len(self.ca_window.my_painting) > 1:
        #     caio.create_PNG_map(self.ca_window.my_painting, self.ca_window.COLORS)
        # else:
        #     print("The image hasn't been generated")

    def incl_cnt_enter(self):
        print("incl_cnt_enter clicked")
        temp = int(self.incl_cnt_le.text())
        if temp > 10000:
            temp = 10000
        if temp < 0:
            temp = 0
        self.ca_sim_inclusions = temp
        print(self.ca_sim_inclusions)
        self.ca_sim.set_inclusion_cnt(temp)
        print(self.ca_sim.get_inclusion_cnt())
        self.ui_update()

    def incl_max_rad_enter(self):
        print("incl_max_rad_enter clicked")
        temp = float(self.max_rad_le.text())
        print("temp val:", temp)
        if temp > self.MAX_RADIUS:
            temp = self.MAX_RADIUS
        if temp < self.min_radius:
            temp = self.min_radius + 1
        self.max_radius = temp
        print(self.max_radius)
        self.ca_sim.set_inclusion_max_radius(temp)
        print(self.ca_sim.get_inclusion_max_radius())
        self.ui_update()

    def incl_min_rad_enter(self):
        print("incl_min_rad_enter clicked")
        temp = float(self.min_rad_le.text())
        print("temp val:", temp)
        if temp > self.max_radius or temp < self.MIN_RADIUS:
            temp = self.MIN_RADIUS
        self.min_radius = temp
        print(self.min_radius)
        self.ca_sim.set_inclusion_min_radius(temp)
        print(self.ca_sim.get_inclusion_min_radius())
        self.ui_update()

    def gbc_clicked(self):
        print("gbc_clicked")
        if self.gbc_chkbox.isChecked() is True:
            print("checked")
            self.ca_sim.set_neighborhood_method("Moore")
            self.ui_update()
            print("get_nei:", self.ca_sim.get_neighborhood_method())
            self.method_cb.setDisabled(True)
            self.ca_sim.set_gbc(True)
        else:
            self.method_cb.setEnabled(True)
            self.ca_sim.set_gbc(False)
            print("unchecked")
        self.ui_update()

    def change_probab_enter(self):
        print("change_probab_enter")
        try:
            temp = int(self.change_probab_le.text())
        except:
            print("Wrong value!")
        print("temp val:", temp)
        if temp > 100 or temp < 0:
            self.ca_sim_change_probab = 50
            self.ca_sim.set_change_probability(50)
        else:
            self.ca_sim_change_probab = temp
            self.ca_sim.set_change_probability(temp)
        self.ui_update()

    def mousePressEvent(self, e):
        x = e.x()
        y = e.y()
        if 10 <= x <= 510 and 30 <= y <= 530:
            x -= 10
            y -= 30
            print("Mouse coords:", x, y)
            if x < self.ca_sim.get_painting_width() and y < self.ca_sim.get_painting_hight():
                if self.ca_sim.remove_grain_by_pos(x, y) is True:
                    # self.ca_sim.get_boundaries()
                    self.worker = Worker(self.draw_image)
                    self.threadpool.start(self.worker)



    def gen_substr_clicked(self):
        print("gen_substr_clicked")
        self.ca_sim.rand_seeds_substruct()
        self.worker = Worker(self.draw_image)
        self.threadpool.start(self.worker)


    def gen_phase_clicked(self):
        print("gen_phase_clicked")
        self.ca_sim.rand_seeds_new_phase()
        self.worker = Worker(self.draw_image)
        self.threadpool.start(self.worker)
        # self.worker = Worker(self.draw_image)
        # self.threadpool.start(self.worker)

    def calc_boundaries_clicked(self):
        print("calc_boundaries_clicked")
        self.ca_sim.get_boundaries()
        self.worker = Worker(self.draw_image)
        self.threadpool.start(self.worker)
        self.ui_update()

app = QtWidgets.QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec_()
