from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import Qt
from ca_sim import CA_sim
import ca_algorithm2 as ca
import numpy as np
import ca_neighborhood as ca_nei

from typing import List, Tuple, Dict


class CAPainting(QtGui.QPainter):

    pen = QtGui.QPen()
    COLORS = dict()
    my_painting: List[List] = list()
    MAX_WIDTH: int = 100
    MAX_HIGHT: int = 100
    SEED_CNT: int = 200
    NEI_METHOD = ca.VONNEUMANN_NEIGHBOURHOOD
    SHARPBORDER = True
    incl_id = 1
    incl_cnt = 0
    phase_id = 1
    incl_min_radius = 0.5
    incl_max_radius = 40
    ca_sim = CA_sim()

    def draw_point(self,x_pos, y_pos):
        # print("Entered: draw_point")
        self.set_pen_width(1)
        # print("--->draw_point: pen width set to 1 ")
        self.drawPoint(x_pos, y_pos)
        # print("--->draw_point: drawPoint")

    def draw_wide_point(self, x_pos, y_pos, width):
        self.set_pen_width(width)
        self.drawPoint(x_pos, y_pos)

    def set_pen_color(self, color: Tuple):
        # print("Entered: set_pen_color")
        self.pen.setColor(QtGui.QColor(color[0],color[1],color[2]))
        # print("--->set_pen_color: pen color set ")
        self.setPen(self.pen)
        # print("--->set_pen_color: pen set")

    def set_pen_width(self, width: int):
        # print("Entered: set_pen_width")
        self.pen.setWidth(width)
        # print(f"--->set_pen_width: pen width set: {width}")
        self.setPen(self.pen)
        # print("--->set_pen_width: pen set ")

    def clear_painting(self):
        self.set_pen_width(1000)
        self.set_pen_color((255,255,255))
        self.drawLine(0, 0, 1000, 1000)

    def colors_rand(self, colors_cnt):
        self.COLORS = ca.color_dict_rand(colors_cnt)
        # print(self.COLORS)
        # print("key(23): ",ca.color_distrib(23,self.COLORS))

    def set_painting_width(self, width):
        self.MAX_WIDTH = width

    def set_painting_hight(self, hight):
        self.MAX_HIGHT = hight

    def set_seed_cnt(self, seed_cnt):
        self.SEED_CNT = seed_cnt

    def set_incl_cnt(self, incl_cnt):
        self.incl_cnt = incl_cnt

    def set_incl_min_rad(self, incl_min_rad):
        self.incl_min_radius = incl_min_rad

    def set_incl_max_rad(self, incl_max_rad):
        self.incl_max_radius = incl_max_rad

    def set_neighbourhood(self, method: str):
        if "Moore" == method:
            self.ca_sim.set_neighborhood_method(ca_nei.MOORE)
            print("Moore: ")
            print(self.ca_sim.get_neighborhood_method())
        elif "von Neumann" == method:
            self.ca_sim.set_neighborhood_method(ca_nei.VONNEUMANN)
            print("von Neumann: ")
            print(self.ca_sim.get_neighborhood_method())
        else:
            print("NO method like this")

    def set_border_sharp(self, isSharp: bool):
        self.SHARPBORDER = isSharp

    def ca_paint_draw(self, myPainting: List[List[int]]):
        len_width = len(myPainting)
        len_hight = len(myPainting[0])
        # print("Entered: ca_paint_draw")
        for y_pos in range(0, len_hight):
            # print(f"--->ca_paint_draw: y_pos: {y_pos}")
            for x_pos in range(0, len_width):
                # print(f"------>ca_paint_draw: x_pos: {x_pos}")
                rgb_color = ca.color_distrib(self.my_painting[x_pos][y_pos], self.COLORS)
                # print(rgb_color)
                self.set_pen_color(rgb_color)
                self.draw_point(x_pos,y_pos)
                # print("x_pos iter done")

    def ca_paint_clear_window(self):
        self.my_painting = ca.matrix_init(self.MAX_WIDTH, self.MAX_HIGHT)
        self.ca_paint_draw(self.my_painting)

    def ca_paint_gen_seeds(self):
        print("Weszło")
        self.my_painting = ca.matrix_init(self.MAX_WIDTH, self.MAX_HIGHT)
        print("Przeszło init")
        self.COLORS = ca.color_dict_rand(self.SEED_CNT)
        print("Przeszło color rand")
        inclusion_pos = ca.matrix_incl_rand(self.my_painting,self.incl_cnt,self.incl_min_radius, self.incl_max_radius)
        print("Wydupiło")
        ca.matrix_random_init(self.my_painting, self.SEED_CNT,inclusion_pos)
        print("Wydupiło 2")
        self.ca_paint_draw(self.my_painting)

    def ca_paint_calc_next_gen(self):
        empty_cells_cnt = ca.grain_next_gen(self.my_painting, self.NEI_METHOD, self.SHARPBORDER)
        self.ca_paint_draw(self.my_painting)
        #(my_matrix: List[List], nei_method: Tuple[Tuple], sharp_border: int)
        return empty_cells_cnt

    def ca_paint_play(self):
        while(self.ca_paint_calc_next_gen()):
            # print("next iter")
            pass