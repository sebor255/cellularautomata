from PyQt5 import QtGui
from ca_sim import CASim
import time


from typing import List, Tuple


class CAPainting(QtGui.QPainter):

    def __init__(self, a0):
        super().__init__(a0)
        self.pen = QtGui.QPen()
        self.my_painting = []

    def draw_point(self, x_pos, y_pos):
        self.set_pen_width(1)
        self.drawPoint(x_pos, y_pos)

    def draw_wide_point(self, x_pos, y_pos, width):
        self.set_pen_width(width)
        self.drawPoint(x_pos, y_pos)

    def set_pen_color(self, color: Tuple):
        self.pen.setColor(QtGui.QColor(color[0], color[1], color[2]))
        self.setPen(self.pen)

    def set_pen_width(self, width: int):
        self.pen.setWidth(width)
        self.setPen(self.pen)

    def clear_painting(self):
        self.set_pen_width(1000)
        self.set_pen_color((255, 255, 255))
        self.drawLine(0, 0, 1000, 1000)

    # def ca_paint_draw(self, my_painting: List[List]):
    #     len_width = len(my_painting)
    #     len_hight = len(my_painting[0])
    #     for y_pos in range(0, len_hight):
    #         for x_pos in range(0, len_width):
    #             rgb_color = my_painting[x_pos][y_pos]
    #             self.set_pen_color(rgb_color)
    #             self.draw_point(x_pos, y_pos)

    def ca_paint_draw(self, ca_sim: CASim):
        print("worker started")
        try:
            # self.clear_painting()             # TODO: add clearing the window
            # time1 = time.time_ns()
            current_generation = ca_sim.get_phase_id()
            len_width = ca_sim.get_painting_width()
            # print("len_width =", len_width)
            len_hight = ca_sim.get_painting_hight()
            # print("len_hight =", len_hight)
            # print("after geting dimentions:", float((time.time_ns() - time1) / pow(1000, 3)))
            for y_pos in range(0, len_hight):
                for x_pos in range(0, len_width):
                    # print("x_pos = {}, y_pos = {}".format(x_pos, y_pos))
                    # print("---> ----> start inner loop:", float((time.time_ns() - time1) / pow(1000, 3)))
                    incl_id = ca_sim.board.get_cell_inclusion_id(x_pos, y_pos)
                    if incl_id == 0:
                        phase_id = ca_sim.board.get_cell_phase_id(x_pos, y_pos)
                        if current_generation > 1 and current_generation != phase_id:                     # change all grains with old phase id
                            # print("phase_id =", phase_id)
                            rgb_color = ca_sim.phase_colors.get_color_by_id(phase_id)
                            # print("---->rgb_color =", rgb_color)
                        else:
                            grain_id = ca_sim.board.get_cell_grain_id(x_pos, y_pos)
                            # print("currently the newest phase")
                            rgb_color = ca_sim.colors.get_color_by_id(grain_id)
                            # print("rgb_color =", rgb_color)
                        # print("rgb_color =", rgb_color)
                    else:
                        # print("INCLUSION")
                        rgb_color = (0, 0, 0)                   # inclusion color
                    # print("---> after get_cell_color:", float((time.time_ns() - time1) / pow(1000, 3)))
                    self.set_pen_color(rgb_color)
                    # print("---> after set_pen_color:", float((time.time_ns() - time1) / pow(1000, 3)))
                    self.draw_point(x_pos, y_pos)
                    # print("after one column:", float((time.time_ns() - time1) / pow(1000, 3)))
                # print("after one column:", float((time.time_ns() - time1) / pow(1000, 3)))
            # print("paint has ben drawn")
            # time.sleep(0.1)  # sleep for 10 ms
        except:
            print("ca_paint_draw jebło")