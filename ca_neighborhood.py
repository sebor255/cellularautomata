MOORE = ((-1, -1), (0, -1), (+1, -1),
         (-1, 0),           (+1, 0),
         (-1, +1), (0, +1), (+1, +1))

VONNEUMANN = (         (0, -1),
              (-1, 0),          (+1, 0),
                       (0, +1))

NEAREST_MOORE = VONNEUMANN

FURTHER_MOORE = ((-1, -1),          (+1, -1),

                 (-1, +1),          (+1, +1))

class Neighborhood:

    def __init__(self):
        self.method = VONNEUMANN

    def set_neighborhood(self, method_type):
        self.method = method_type

    def get_neighborhood(self):
        return self.method

    #TODO: define more methods
