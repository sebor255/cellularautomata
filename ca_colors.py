from typing import Dict, Tuple
import random
from ca_cell import Cell


class Colors:
    def __init__(self, grain_cnt):
        self.colors_dict = {}
        self.colors_rand(grain_cnt)

    # returns dict of colors in type: (R,G,B)
    def colors_rand(self, colors_cnt: int):
        colors = dict()
        colors.update({0: (255, 255, 255)})                         #default color has to be always white
        for x in range(colors_cnt):
            colors.update({(x+1): (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))})
        self.colors_dict = colors

    def get_colors_dict(self) -> Dict:
        return self.colors_dict

    def get_color_by_cell(self, cell: Cell) -> Tuple:
        if cell.get_inclusion_id() == 0:
            return self.colors_dict.get(cell.grain_id, None)
        else:
            return 0, 0, 0           # black color

    def get_color_by_id(self, color_id: int) -> Tuple:      # TODO: here can be bug
        return self.colors_dict.get(color_id, (0, 0, 0))

    def set_color_by_id(self, grain_id: int, color_rgb: Tuple):
        self.colors_dict.update({grain_id: color_rgb})

    def print_colors(self):
        for color in self.colors_dict.items():
            print(color)

    def add_random_colors(self, start_grain_id, count):
        for x in range(count):
            # print()
            temp = start_grain_id + x
            self.colors_dict.update({(temp+1): (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))})


# cd = Colors(10)
# cd.print_colors()
# cd.colors_rand(5)
# cd.print_colors()
#
