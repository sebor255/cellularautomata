from ca_board import Board
from ca_colors import Colors
import random
import ca_neighborhood as ca_nei
import copy


class CASim:
    def __init__(self, width=500, hight=500, seed_cnt=10, incl_cnt=0, incl_id=1, phase_id=1, min_rad=0.5, max_rad=5.5,
                 sharp_border=True):
        self.NO_GRAIN_ID = 0
        self.NO_INCL_ID = 0
        self.NO_PHASE_ID = 0
        self.board = Board(width, hight)
        self.seed_cnt_total = 0
        self.seed_cnt = seed_cnt
        self.seeds_newly_added = 0
        self.incl_cnt = incl_cnt
        self.incl_id = incl_id
        self.phase_id = phase_id
        self.min_rad = min_rad
        self.max_rad = max_rad
        self.colors = Colors(seed_cnt)  # seeds_cnt determines no. of colors
        self.phase_colors = Colors(phase_id)
        self.neighborhood = ca_nei.Neighborhood()
        self.initiated_pos = set()
        self.empty_cells_pos_list = set()
        self.sharp_border = sharp_border
        self.empty_cells_cnt = 0
        self.change_probab = 50
        self.is_gbc = False
        self.boundaries_pos_list = set()

    # DONE; The function rands "incl_cnt" of inclusions, and adds them with specified radius
    # def __update_initiated_pos(self, x_pos, y_pos):
    #     self.initiated_pos.update({(x_pos, y_pos)})
    #
    # def __remove_initiated_pos(self, x_pos, y_pos):
    #     try:
    #         print(self.initiated_pos)
    #         self.initiated_pos.remove((x_pos, y_pos))
    #     except:
    #         print("__remove_initiated_pos ERROR")

    def __update_empty_cells(self, x_pos, y_pos):
        self.empty_cells_pos_list.update({(x_pos, y_pos)})

    def __remove_empty_cells(self, x_pos, y_pos):
        try:
            self.empty_cells_pos_list.remove((x_pos, y_pos))
        except:
            print("__remove_empty_cells ERROR: x_pos={}, y_pos={}".format(x_pos, y_pos))

    def __update_boundaries_list(self, x_pos, y_pos):
        self.boundaries_pos_list.update({(x_pos, y_pos)})

    def __remove_boundaries_list(self, x_pos, y_pos):
        try:
            self.boundaries_pos_list.remove((x_pos, y_pos))
        except:
            print("__remove_boundaries_list ERROR: x_pos={}, y_pos={}".format(x_pos, y_pos))

    def __get_empty_cells(self):
        print("__get_empty_cells")
        for y_pos in range(self.board.get_hight()):
            for x_pos in range(self.board.get_width()):
                if self.board.get_cell_inclusion_id(x_pos, y_pos) == 0 and self.board.get_cell_phase_id(x_pos, y_pos) == 0:
                    self.__update_empty_cells(x_pos, y_pos)
        # print("self.empty_cells_pos_list", self.empty_cells_pos_list)
        print("len:", len(self.empty_cells_pos_list))
        # print()

    def rand_inclusions(self):
        print("rand_inclusions:")
        print(self.get_inclusion_min_radius())
        initiated_pos = []
        fin_initiated_pos = []
        initiated_radius = []
        self.__get_empty_cells()

        if self.incl_cnt != 0:
            i = 0
            while i < self.incl_cnt:
                x_pos = random.randrange(0, self.board.get_width())
                y_pos = random.randrange(0, self.board.get_hight())
                if (x_pos, y_pos) in self.empty_cells_pos_list:
                    i += 1
                    initiated_pos.append((x_pos, y_pos))
                    #print("min_rad =", self.min_rad, "  max_rad =", self.max_rad)
                    initiated_radius.append(random.uniform(self.min_rad, self.max_rad))
                    self.board.set_cell_inclusion_id(x_pos, y_pos, self.incl_id)
                    self.__remove_empty_cells(x_pos, y_pos)

            for y_pos in range(self.board.get_hight()):
                for x_pos in range(self.board.get_width()):
                    if self.board.get_cell_inclusion_id(x_pos, y_pos) == 0:
                        for init_pos, init_rad in zip(initiated_pos, initiated_radius):
                            init_width, init_hight = init_pos
                            if pow((x_pos - init_width), 2) + pow((y_pos - init_hight), 2) <= pow(init_rad, 2):
                                # fin_initiated_pos.append((x_pos, y_pos))
                                self.board.set_cell_inclusion_id(x_pos, y_pos, self.incl_id)
                                self.__remove_empty_cells(x_pos, y_pos)
            # self.initiated_pos = set(fin_initiated_pos + initiated_pos)
        print("after rand_inclusions empty cells:", len(self.empty_cells_pos_list))
        # print()


    # DONE; The function rands "seed_cnt" of seeds in places other than inclusions
    def rand_seeds(self):
        print("Enetered: rand_seeds")
        grain_id = 0
        iterat = 0
        while grain_id < self.seed_cnt:
            temp_width = random.randrange(0, self.board.get_width())
            temp_hight = random.randrange(0, self.board.get_hight())
            if (temp_width, temp_hight) in self.empty_cells_pos_list:
                grain_id += 1
                # self.__update_initiated_pos(temp_width, temp_hight)
                self.__remove_empty_cells(temp_width, temp_hight)
                self.board.set_cell_grain_id(temp_width, temp_hight, grain_id)
                self.board.set_cell_phase_id(temp_width, temp_hight, self.phase_id)
            iterat += 1

        self.seed_cnt_total = grain_id
        print("after rand_seeds empty cells:", len(self.empty_cells_pos_list))
        print("iterat:", iterat)
        print()

    def rand_seeds_substruct(self):
        print("Enetered: rand_seeds_substruct")
        # self.set_phase_id(self.get_phase_id()+1)
        self.seed_cnt_total += self.seed_cnt
        print("TOTAL:", self.seed_cnt_total)
        grain_id = self.seed_cnt_total - self.seed_cnt  + 1
        self.seeds_newly_added = self.seed_cnt
        print("self.seeds_newly_added =", self.seeds_newly_added)
        print("grain_id =", grain_id)
        self.colors.add_random_colors(grain_id, self.seeds_newly_added)

        if len(self.empty_cells_pos_list) > 0:
            while grain_id <= self.seed_cnt_total:
                print("grain_id =", grain_id)
                temp = len(self.empty_cells_pos_list)
                elem_pos = random.randint(0, temp)
                my_list = list(self.empty_cells_pos_list)
                elem = my_list[elem_pos]
                temp_width = elem[0]
                temp_hight = elem[1]
                grain_id += 1
                self.__remove_empty_cells(temp_width, temp_hight)
                self.board.set_cell_grain_id(temp_width, temp_hight, grain_id)
                self.board.set_cell_phase_id(temp_width, temp_hight, self.phase_id)
        else:
            print("rand_seeds_substruct: empty_cells_pos_list len less than 0")
        print("after rand_seeds empty cells:", len(self.empty_cells_pos_list))

    def rand_seeds_new_phase(self):
        try:
            print("Enetered: rand_seeds_substruct")
            self.set_phase_id(self.get_phase_id()+1)
            print("actual phase id:", self.get_phase_id())
            self.seed_cnt_total = self.seed_cnt
            print("TOTAL:", self.seed_cnt_total)
            grain_id = 1
            self.seeds_newly_added = self.seed_cnt
            print("self.seeds_newly_added =", self.seeds_newly_added)
            print("grain_id =", grain_id)
            self.colors = Colors(self.seed_cnt)
            self.phase_colors.add_random_colors(self.phase_id, 1)

            if len(self.empty_cells_pos_list) > 0:
                while grain_id <= self.seed_cnt_total:
                    print("grain_id =", grain_id)
                    temp = len(self.empty_cells_pos_list)
                    elem_pos = random.randint(0, temp)
                    my_list = list(self.empty_cells_pos_list)
                    elem = my_list[elem_pos]
                    temp_width = elem[0]
                    temp_hight = elem[1]
                    grain_id += 1
                    self.__remove_empty_cells(temp_width, temp_hight)
                    self.board.set_cell_grain_id(temp_width, temp_hight, grain_id)
                    self.board.set_cell_phase_id(temp_width, temp_hight, self.phase_id)
            else:
                print("rand_seeds_substruct: empty_cells_pos_list len less than 0")
            print("after rand_seeds empty cells:", len(self.empty_cells_pos_list))
        except:
            print("rand_seeds_new_phase exception")
    # returns list of cells being neighbours of the cell in pos x_pos, y_pos
    def __position_by_border(self, x_pos, y_pos):
        nei_cells = []
        for nei_cell_pos in self.neighborhood.method:
            # print(nei_cell_pos, "args:", (x_pos, y_pos))
            if self.sharp_border:
                x_temp = x_pos + nei_cell_pos[0]
                y_temp = y_pos + nei_cell_pos[1]
                if 0 <= x_temp < self.board.get_width() and 0 <= y_temp < self.board.get_hight():
                    nei_cells.append((x_temp, y_temp))
                    # print("---> nei_cells list:", nei_cells)
            elif not self.sharp_border:
                x_temp = (x_pos - nei_cell_pos[0]) % self.board.get_width()
                y_temp = (y_pos - nei_cell_pos[1]) % self.board.get_hight()
                nei_cells.append((x_temp, y_temp))
        return nei_cells

    # 0 is possible to return
    def __get_most_freq_id(self, x_pos, y_pos, last_board):
        nei_pos_list = self.__position_by_border(x_pos, y_pos)
        nei_grain_id_list = self.__get_nei_grain_ids(nei_pos_list, last_board)
        set_id = 0
        most_frequent = 0
        unique_values = set(nei_grain_id_list)
        if len(nei_grain_id_list) > 0:
            for val in unique_values:
                if val != self.NO_GRAIN_ID:  # when cell has grain ID != 0
                    frequence = nei_grain_id_list.count(val)
                    # print(frequence)
                    if frequence == most_frequent:
                        set_id = random.choice([set_id, val])  # if there is the same frequence of 2 IDs choice one
                    elif frequence > most_frequent:
                        most_frequent = frequence
                        set_id = val  # set the most frequent ID
        # print("set_id:", set_id)
        return set_id, nei_grain_id_list

    # Takes list of neighbors and returns list of grain ids
    def __get_nei_grain_ids(self, nei_pos_list, last_board):
        nei_grain_ids = []
        for pos in nei_pos_list:
            grain_id = last_board[pos[0]][pos[1]].get_grain_id()
            if self.seed_cnt_total != self.seed_cnt:            # needed for substructure
                if grain_id <= self.seed_cnt_total - self.seeds_newly_added:
                    continue
            if last_board[pos[0]][pos[1]].get_phase_id() == self.phase_id:
                nei_grain_ids.append(last_board[pos[0]][pos[1]].get_grain_id())
        # print("nei_grain_ids:", nei_grain_ids)
        return nei_grain_ids

    # calculates next generation of board
    def next_gen(self):
        last_board = self.board.get_copy()
        temp_pos_list = copy.deepcopy(self.empty_cells_pos_list)
        for pos in temp_pos_list:
            x_pos = pos[0]
            y_pos = pos[1]
            if self.is_gbc is False:
                if self.__default_algorithm(x_pos, y_pos, last_board) is True:
                    self.__remove_empty_cells(x_pos, y_pos)
                    continue
            elif self.is_gbc is True:
                if self.__rule1(x_pos, y_pos, last_board) is False:
                    if self.__rule2(x_pos, y_pos, last_board) is False:
                        if self.__rule3(x_pos, y_pos, last_board) is False:
                            if self.__rule4(x_pos, y_pos, last_board) is False:
                                pass
            else:
                print("No such algorithm! gbc has been set with wrong value")
        # print("LEN OF self.empty_cells_pos_list:", len(self.empty_cells_pos_list))
        # print("self.empty_cells_pos_list:", self.empty_cells_pos_list)
        print("self.empty_cells_cnt len:", len(self.empty_cells_pos_list))
        return len(self.empty_cells_pos_list)

    def __rule1(self, x_pos, y_pos, last_board):
        self.neighborhood.set_neighborhood(ca_nei.MOORE)
        nei_pos_list = self.__position_by_border(x_pos, y_pos)
        nei_grain_id_list = self.__get_nei_grain_ids(nei_pos_list, last_board)
        if len(nei_grain_id_list) is 0:
            return False
        most_frequent_id = 0
        biggest_frequence = 0
        unique_values = set(nei_grain_id_list)
        if len(nei_grain_id_list) > 0:
            for unique_val in unique_values:
                if unique_val != self.NO_GRAIN_ID:  # when cell has grain ID != 0
                    frequence_of_unique_val = nei_grain_id_list.count(unique_val)
                    # print(frequence)
                    if frequence_of_unique_val == biggest_frequence:
                        most_frequent_id = random.choice([most_frequent_id, unique_val])  # if there is the same frequence of 2 IDs choice one
                    elif frequence_of_unique_val > biggest_frequence:
                        biggest_frequence = frequence_of_unique_val
                        most_frequent_id = unique_val  # set the most frequent ID
        if most_frequent_id != self.NO_GRAIN_ID:
            if biggest_frequence >= 5:
                self.board.set_cell_grain_id(x_pos, y_pos, most_frequent_id)
                self.board.set_cell_phase_id(x_pos, y_pos, self.phase_id)
                self.__remove_empty_cells(x_pos, y_pos)
                return True
        return False

    def __rule2(self, x_pos, y_pos, last_board):
        self.neighborhood.set_neighborhood(ca_nei.NEAREST_MOORE)
        nei_pos_list = self.__position_by_border(x_pos, y_pos)
        nei_grain_id_list = self.__get_nei_grain_ids(nei_pos_list, last_board)
        if len(nei_grain_id_list) is 0:
            return False
        most_frequent_id = 0
        biggest_frequence = 0
        unique_values = set(nei_grain_id_list)
        if len(nei_grain_id_list) > 0:
            for unique_val in unique_values:
                if unique_val != self.NO_GRAIN_ID:  # when cell has grain ID != 0
                    frequence_of_unique_val = nei_grain_id_list.count(unique_val)
                    # print(frequence)
                    if frequence_of_unique_val == biggest_frequence:
                        most_frequent_id = random.choice([most_frequent_id, unique_val])  # if there is the same frequence of 2 IDs choice one
                    elif frequence_of_unique_val > biggest_frequence:
                        biggest_frequence = frequence_of_unique_val
                        most_frequent_id = unique_val  # set the most frequent ID
        if most_frequent_id != self.NO_GRAIN_ID:
            if biggest_frequence >= 3:
                self.board.set_cell_grain_id(x_pos, y_pos, most_frequent_id)
                self.board.set_cell_phase_id(x_pos, y_pos, self.phase_id)
                self.__remove_empty_cells(x_pos, y_pos)
                return True
        return False

    def __rule3(self, x_pos, y_pos, last_board):
        self.neighborhood.set_neighborhood(ca_nei.FURTHER_MOORE)
        nei_pos_list = self.__position_by_border(x_pos, y_pos)
        nei_grain_id_list = self.__get_nei_grain_ids(nei_pos_list, last_board)
        if len(nei_grain_id_list) is 0:
            return False
        most_frequent_id = 0
        biggest_frequence = 0
        unique_values = set(nei_grain_id_list)
        if len(nei_grain_id_list) > 0:
            for unique_val in unique_values:
                if unique_val != self.NO_GRAIN_ID:  # when cell has grain ID != 0
                    frequence_of_unique_val = nei_grain_id_list.count(unique_val)
                    # print(frequence)
                    if frequence_of_unique_val == biggest_frequence:
                        most_frequent_id = random.choice([most_frequent_id, unique_val])  # if there is the same frequence of 2 IDs choice one
                    elif frequence_of_unique_val > biggest_frequence:
                        biggest_frequence = frequence_of_unique_val
                        most_frequent_id = unique_val  # set the most frequent ID
        if most_frequent_id != self.NO_GRAIN_ID:
            if biggest_frequence >= 3:
                self.board.set_cell_grain_id(x_pos, y_pos, most_frequent_id)
                self.board.set_cell_phase_id(x_pos, y_pos, self.phase_id)
                self.__remove_empty_cells(x_pos, y_pos)
                return True
        return False

    def __rule4(self, x_pos, y_pos, last_board):
        self.neighborhood.set_neighborhood(ca_nei.MOORE)
        nei_pos_list = self.__position_by_border(x_pos, y_pos)
        nei_grain_id_list = self.__get_nei_grain_ids(nei_pos_list, last_board)
        if len(nei_grain_id_list) is 0:
            return False
        most_frequent_id = 0
        biggest_frequence = 0
        unique_values = set(nei_grain_id_list)
        if len(nei_grain_id_list) > 0:
            for unique_val in unique_values:
                if unique_val != self.NO_GRAIN_ID:  # when cell has grain ID != 0
                    frequence_of_unique_val = nei_grain_id_list.count(unique_val)
                    # print(frequence)
                    if frequence_of_unique_val == biggest_frequence:
                        most_frequent_id = random.choice([most_frequent_id, unique_val])  # if there is the same frequence of 2 IDs choice one
                    elif frequence_of_unique_val > biggest_frequence:
                        biggest_frequence = frequence_of_unique_val
                        most_frequent_id = unique_val  # set the most frequent ID
        if most_frequent_id != self.NO_GRAIN_ID:
            if random.randint(1, 100) <= self.change_probab:
                self.board.set_cell_grain_id(x_pos, y_pos, most_frequent_id)
                self.board.set_cell_phase_id(x_pos, y_pos, self.phase_id)
                self.__remove_empty_cells(x_pos, y_pos)
                return True
        return False


    def __default_algorithm(self, x_pos, y_pos, last_board):
        most_freq_id, nei_grain_id_list = self.__get_most_freq_id(x_pos, y_pos, last_board)
        if most_freq_id != self.NO_GRAIN_ID:
            self.board.set_cell_grain_id(x_pos, y_pos, most_freq_id)
            self.board.set_cell_phase_id(x_pos, y_pos, self.phase_id)
            return True
        else:
            return False

    def get_boundaries(self):
        print("get_boundaries entered")
        for y_pos in range(self.board.get_hight()):
            for x_pos in range(self.board.get_width()):
                flag_dif_r = 0
                flag_dif_d = 0
                grain_id = self.board.get_cell_grain_id(x_pos, y_pos)
                grain_id_r = self.board.get_cell_grain_id(x_pos + 1, y_pos)
                grain_id_d = self.board.get_cell_grain_id(x_pos, y_pos + 1)

                if grain_id != grain_id_r:
                    flag_dif_r = 1
                    print("Right different")
                if grain_id != grain_id_d:
                    flag_dif_d = 1
                    print("Down different")
                if flag_dif_d or flag_dif_r:
                    print("incl id changed")
                    self.board.set_cell_inclusion_id(x_pos, y_pos, 1)                  # TODO: DONE: the idea is that phase id is always black so we can assume those are inclusions :)
                    self.__update_boundaries_list(x_pos, y_pos)

                # for x in range(self._ca_algo.space_width):
                #     for y in range(self._ca_algo.space_width):
                #         c = self._ca_algo.space[x, y]
                #         if x < self._ca_algo.space_width - 1:
                #             c1 = self._ca_algo.space[x + 1, y]
                #         else:
                #             c1 = c
                #         if y < self._ca_algo.space_width - 1:
                #             c2 = self._ca_algo.space[x, y + 1]
                #         else:
                #             c2 = c
                #         if c > 1 and (c != c1 or c != c2):
                #             self._ca_algo.space[x, y] = 1


    # PUBLIC: set nei method
    def set_neighborhood_method(self, nei_method):
        try:
            if "Moore" == nei_method:
                self.neighborhood.set_neighborhood(ca_nei.MOORE)
            elif "von Neumann" == nei_method:
                self.neighborhood.set_neighborhood(ca_nei.VONNEUMANN)
            else:
                print("NO method like this")
        except:
            print("Neighborhood method set wrongly")

    def get_neighborhood_method(self):
        if self.neighborhood.get_neighborhood() == ca_nei.VONNEUMANN:
            return "von Neumann"
        elif self.neighborhood.get_neighborhood() == ca_nei.MOORE:
            return "Moore"

    def remove_grain_by_pos(self, x_pos, y_pos):
        grain_id = self.board.get_cell_grain_id(x_pos, y_pos)
        phase_id = self.board.get_cell_phase_id(x_pos, y_pos)
        print("jebło?")
        if phase_id != self.NO_PHASE_ID:
            for y_pos in range(self.board.get_hight()):
                for x_pos in range(self.board.get_width()):
                    if self.board.get_cell_grain_id(x_pos, y_pos) == grain_id and self.board.get_cell_phase_id(x_pos, y_pos) == phase_id:
                        self.board.set_cell_grain_id(x_pos, y_pos, self.NO_GRAIN_ID)
                        self.board.set_cell_phase_id(x_pos, y_pos, self.NO_PHASE_ID)
                        self.__update_empty_cells(x_pos, y_pos)
            print("Grain removed. ID:", grain_id, ", Phase:", phase_id)
            return True
        else:
            return False

    # PUBLIC: set sharp border
    def set_sharp_border(self, is_sharp):
        self.sharp_border = is_sharp

    # PUBLIC: get sharp border
    def get_sharp_border(self):
        return self.sharp_border

    # PUBLIC: returned matrix has format:  color_matrix[x_pos][y_pos]
    # def get_color_matrix(self):
    #     color_matrix = []
    #     if self.phase_id > 1:
    #         self.phase_colors = Colors(self.phase_id - 1)
    #
    #     for x_pos in range(0, self.board.get_width()):
    #         column = []
    #         for y_pos in range(0, self.board.get_hight()):
    #             color = self.board.get_cell_color(x_pos, y_pos, self.colors)
    #             print("color", color)
    #             phase_id = self.board.get_cell_phase_id(x_pos, y_pos)
    #             print("phase_id", phase_id)
    #             if self.phase_id != phase_id and phase_id != 0:
    #                 print("change color to phase id")
    #                 color = self.phase_colors.get_color_by_id(phase_id)
    #             column.append(color)
    #         color_matrix.append(column)
    #     #print(color_matrix)
    #     return color_matrix

    def get_cell_color(self, x_pos, y_pos):
        return self.board.get_cell_color(x_pos, y_pos, self.colors)

    def set_painting_hight(self, hight):
        self.board.set_hight(hight)
        self.board.init_board()

    def get_painting_hight(self):
        return self.board.get_hight()

    def set_painting_width(self, width):
        self.board.set_width(width)
        print("debug: board set")
        self.board.init_board()

    def get_painting_width(self):
        return self.board.get_width()

    def set_seed_cnt(self, seed_cnt):
        self.seed_cnt = seed_cnt
        # self.colors = Colors(seed_cnt)  # seeds_cnt determines no. of colors

    def get_seed_cnt(self):
        return self.seed_cnt

    def set_inclusion_cnt(self, incl_cnt):
        self.incl_cnt = incl_cnt

    def get_inclusion_cnt(self):
        return self.incl_cnt

    def set_inclusion_max_radius(self, radius):
        self.max_rad = radius
        print("max rad set:", self.max_rad)

    def get_inclusion_max_radius(self):
        return self.max_rad

    def set_inclusion_min_radius(self, radius):
        self.min_rad = radius
        print("min rad set:", self.min_rad)

    def get_inclusion_min_radius(self):
        return self.min_rad

    def get_empty_cell_value(self):
        return len(self.empty_cells_pos_list)

    def set_change_probability(self, probability):
        self.change_probab = probability

    def get_change_probability(self):
        return self.change_probab

    def set_gbc(self, is_gbc):
        self.is_gbc = is_gbc

    def set_phase_id(self, phase_id):
        self.phase_id = phase_id

    def get_phase_id(self):
        return self.phase_id

    def get_boundary_len(self):
        return len(list(self.boundaries_pos_list))